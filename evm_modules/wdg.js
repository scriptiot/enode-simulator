var require = globalThis._require;
var nwdg = require("@native.wdg");

function Wdg(){
    
    this.feed = function(){
        nwdg.feed();
    };

    this.close = function () {
        nwdg.close();
    };
}

function open(timeout) {
    if (timeout == undefined || typeof timeout != "number") return undefined;
    if (nwdg.open(timeout) == 0) {
        return new Wdg();
    }
    return undefined;
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        open: open,
    };
}
