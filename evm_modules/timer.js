var require = globalThis._require;
var ntimer = require("@native.timer");
require("eventbus.js");

function HWTIMER(id) {
    this.id = id;
    this.__id__ = -1
    this.setTimeout = function (callback, timeout) {
        this.__id__ = globalThis.eventbus.on(this, "timeout", callback);
        ntimer.setTimeout(this.id, this.__id__, timeout);
    };

    this.setInterval = function (callback, timeout) {
        this.__id__ = globalThis.eventbus.on(this, "interval", callback);
        ntimer.setInterval(this.id, this.__id__, timeout);
    };

    this.close = function () {
        ntimer.close(this.id, this.__id__);
    };
}

function open(id) {
    if (id == undefined || typeof id != "number") return undefined;
    return new HWTIMER(id);
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        open: open,
    };
}
