var require = globalThis._require;
var dev = require("@native.device");
var sys = require("@native.builtin");

function imei() {
    var cp_buff = sys.malloc(128);
    if (cp_buff == null) return undefined;
    var imei = dev.imei(cp_buff, 128);
    if (imei < 0) return undefined;
    var buffer = sys.toJsString(cp_buff, 128);
    sys.free(cp_buff);
    return buffer;
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        imei: imei,
        delay: delay,
        platform: platform,
    };
}
