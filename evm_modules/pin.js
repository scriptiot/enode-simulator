var require = globalThis._require;
var npin = require("@native.pin");

//dir
var IN = 0;
var OUT = 1;

//pull
var NOPULL = 0;
var PULLUP = 1;
var PULLDOWN = 2;

//level
var LOW = 0;
var HIGH = 1;

function setup(index, dir, pull, level) {
    return npin.setup(index, dir, pull, level);
}

function write(index, level) {
    return npin.write(index, level);
}

function read(index) {
    var level = npin.read(index);
    if (level == 0) return LOW;
    if (level == 1) return HIGH;
    return undefined;
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        setup: setup,
        read: read,
        write: write,
    };
}
