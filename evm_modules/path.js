function basename(path) {
    var new_path = path.replace("\\\\", "\\");
    new_path = new_path.replace("\\", "/");
    var path_array = new_path.split("\\");
    return path_array[path_array.length - 1];
}

function dirname(path) {
    var new_path = path.replace("\\\\", "\\");
    new_path = new_path.replace("\\", "/");
    var path_array = new_path.split("\\");
    return path_array[path_array.length - 2];
}

function isAbsolute(path) {
    var new_path = path.replace("\\\\", "\\");
    new_path = new_path.replace("\\", "/");
    var path_array = new_path.split("\\");
    return path_array[0].indexOf("/") == 0;
}

function join(path) {

}

function normalize(path) {

}

function relative(from, to) {
    
}

function resolve(path) {

}

if (globalThis.platform == "nodejs") {
    module.exports = {
        basename: basename,
        dirname: dirname,
        isAbsolute: isAbsolute,
        join: join,
        normalize: normalize,
        relative: relative,
        resolve: resolve
    }
}