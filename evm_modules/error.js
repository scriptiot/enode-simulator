function ThrowError(filename) {
    this.filename = filename;

    this.trace = function(funcname, line, type, message) {
        var error1_trace = "Js Traceback(most recent call last): \n";
        var error_file = "\tFile <" + this.filename + "> " + "line " + line + " <module>\n";
        var error_function = "\tFunction <" + funcname + "> '\n ";
        var error_message = "\t" + type + ":" + message + "\n";
        var message = error1_trace + error_file + error_function + error_message;
        console.log(message);
        return message;
    }
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        ThrowError: ThrowError,
    };
}
