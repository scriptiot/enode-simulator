var require = globalThis._require;
var nrtc = require("@native.rtc");

function RTC() {
    this.setTime = function (options) {
        var year = options["year"];
        var month = options["month"];
        var date = options["date"];
        var hours = options["hours"];
        var minutes = options["minutes"];
        var seconds = options["seconds"];

        if (
            year == undefined ||
            month == undefined ||
            date == undefined ||
            hours == undefined ||
            minutes == undefined ||
            seconds == undefined ||
            typeof year != "number" ||
            typeof month != "number" ||
            typeof date != "number" ||
            typeof hours != "number" ||
            typeof minutes != "number" ||
            typeof seconds != "number"
        )
            return undefined;
        return nrtc.setTime(year, month, date, hours, minutes, seconds);
    };

    this.getTime = function () {
        var date = nrtc.createDate();
        if (date == null) return undefined;
        var ret = nrtc.getTime(date);
        if (ret < 0) {
            nrtc.destoryDate(date);
            return undefined;
        }
        var time = {};
        time["year"] = nrtc.getFullYear(date);
        time["month"] = nrtc.getMonth(date);
        time["day"] = nrtc.getDate(date);
        time["hours"] = nrtc.getHours(date);
        time["minutes"] = nrtc.getMinutes(date);
        time["seconds"] = nrtc.getSeconds(date);
        nrtc.destoryDate(date);
        return time;
    };

    this.close = function () {
        nrtc.close();
    };
}

function open() {
    return new RTC();
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        open: open,
    };
}
