var require = globalThis._require;
var npin = require("@native.pin");
require("eventbus.js");

//mode
var IN = 0;
var OUT = 1;
var IRQ = 2;
var ANALOG = 3;

//pull
var NOPULL = 0;
var PULLUP = 1;
var PULLDOWN = 2;

//level
var LOW = 0;
var HIGH = 1;

//irq
var EVENT_RISING = "rising";
var EVENT_FALLING = "falling";
var EVENT_BOTH = "both";

function Gpio(id){
    this.__id__ = -1;

    this.id = id;
    this.eventlist = {};

    this.read = function () {
        var level = npin.read(this.id);
        if (level == 0) return LOW;
        if (level == 1) return HIGH;
        return undefined;
    };

    this.write = function (level) {
        if (level == undefined || typeof level != "number") return undefined;
        npin.write(this.id, level);
    };

    this.toggle = function () {
        npin.toggle(this.id);
    };

    this.on = function (event, callback) {
        if (
            event != EVENT_RISING &&
            event != EVENT_RISING &&
            event != EVENT_RISING
        )
            return undefined;
        if (typeof callback != "function") return undefined;

        this.__id__ = globalThis.eventbus.on(this, event, callback);
        npin.on(this.id, this.__id__, event);
    };

    this.close = function () {
        npin.close(this.id);
    };
}

function open(opt) {
    if (opt == undefined) return undefined;

    var index = opt["index"];
    var mode = opt["mode"];
    var pull = opt["pull"];
    var level = opt["level"];

    if (
        index == undefined ||
        mode == undefined ||
        typeof index != "number" ||
        typeof mode != "number"
    )
        return undefined;
    if (pull == undefined || typeof pull != "number") pull = NOPULL;
    if (level == undefined || typeof level != "number") level = LOW;

    if (npin.setup(index, mode, pull, level) >= 0) {
        return new Gpio(index);
    }
    return undefined;
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        open: open,
    };
}
