INCLUDEPATH += $$PWD \
    $$PWD/curl-7.75.0_3-win64-mingw/include

HEADERS += \
    $$PWD/curl-7.75.0_3-win64-mingw/include/curl/curl.h \
    $$PWD/curl-7.75.0_3-win64-mingw/include/curl/curlver.h \
    $$PWD/curl-7.75.0_3-win64-mingw/include/curl/easy.h \
    $$PWD/curl-7.75.0_3-win64-mingw/include/curl/mprintf.h \
    $$PWD/curl-7.75.0_3-win64-mingw/include/curl/multi.h \
    $$PWD/curl-7.75.0_3-win64-mingw/include/curl/options.h \
    $$PWD/curl-7.75.0_3-win64-mingw/include/curl/stdcheaders.h \
    $$PWD/curl-7.75.0_3-win64-mingw/include/curl/system.h \
    $$PWD/curl-7.75.0_3-win64-mingw/include/curl/typecheck-gcc.h \
    $$PWD/curl-7.75.0_3-win64-mingw/include/curl/urlapi.h \
    $$PWD/evue_curl.h

SOURCES += \
    $$PWD/evue_curl.c


win32{
    LIBS += -L$$PWD/curl-7.75.0_3-win64-mingw/bin -lcurl-x64 -lwsock32 -lws2_32
}
unix {
    LIBS += -lpthread -L$$PWD/curl-7.75.0_3-win64-mingw/bin -lcurl
}
