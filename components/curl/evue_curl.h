#ifndef EVUE_CURL_H
#define EVUE_CURL_H

#include <stdio.h>
#include "curl/curl.h"
#include "evm.h"

typedef size_t(* write_cb_t)(void *contents, size_t size, size_t nmemb, void *userp);
int curl_evm(evm_t* e, evm_val_t * request, write_cb_t cb, void *chunk, write_cb_t header_cb, void *headermsg);
int curl_raw(const char *url, const char *method, const char *postfield, struct curl_slist *headers, write_cb_t cb, void * chunk);
int curl(const char *url, const char *method, const char *postfield, const char *header, write_cb_t cb, void * chunk);

#endif
