HEADERS += \
    $$PWD/include/evm.h \
    $$PWD/include/evm_type.h \
    $$PWD/include/evm_module_cffi.h \
    $$PWD/include/ecma.h \
    $$PWD/include/evm_app.h

INCLUDEPATH += $$PWD/include

LIBS += -L$$PWD/./ -lenode

DEPENDPATH += $$PWD/include

