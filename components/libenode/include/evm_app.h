/****************************************************************************
**
** Copyright (C) 2021 @武汉市字节码科技有限公司
**
**  EVM是一款通用化设计的虚拟机引擎，拥有语法解析前端接口、编译器、虚拟机和虚拟机扩展接口框架。
**  支持语言类型：JavaScript, Python, QML, EVUE, JSON, XML, HTML, CSS
**  Version	: 3.0
**  Email	: scriptiot@aliyun.com
**  Website	: https://github.com/scriptiot
**  Licence: 个人免费，企业授权
****************************************************************************/
#ifndef EVUE_H
#define EVUE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

#include "evm.h"

typedef void * (* evm_app_malloc)(size_t size);
typedef void (* evm_app_free)(void * mem);
typedef void (*evm_app_print)(const char *format, ...);
typedef void (* evm_app_destory)();

/**
 * @brief  注册os的 malloc 函数
 * @param  fn, malloc函数指针
 */
void evm_app_register_malloc(evm_app_malloc fn);

/**
 * @brief  注册os的 free 函数
 * @param  fn, free函数指针
 */
void evm_app_register_free(evm_app_free fn);

/**
 * @brief  注册os的 printf 函数
 * @param  fn, printf函数指针
 */
void evm_app_register_printf(evm_app_print fn);

/**
 * @brief  该函数主要用户自定义虚拟机堆、栈和外部模块内存占用
 * @note
 * @param  evm_heap_size: 虚拟机堆空间大小， 原则越大越好
 * @param  evm_stack_size: 虚拟机栈空间大小， 默认100k 即可
 * @param  evm_ext_size: 虚拟机外部内存大小，可以设置位堆空间的 1/2左右
 * @retval None
 * @example evm_app_init(1024 * 1024, 100*1024, 924 * 1024);
 *
 */
evm_t * evm_app_init(uint32_t evm_heap_size, uint32_t evm_stack_size, uint32_t evm_ext_size);


/**
 * @brief   获取虚拟机指针
 */
evm_t * get_evm();

/**
 * @brief   虚拟机状态
 * @note
 * @param
 * @param
 * @param
 * @retval true, 表示虚拟机暂停； false表示虚拟机运行
 * @example
 *
 */
int evm_app_is_paused();

/**
 * @brief   有外部事件来，暂停evue虚拟机的运行的接口（包括暂停界面、声音和网络）
 * @note
 * @param
 * @param
 * @param
 * @retval None
 * @example
 *
 */
void evm_app_pause();

/**
 * @brief   外部事件结束后，恢复evue虚拟机的运行的接口（包括暂停界面、声音和网络）
 * @note
 * @param
 * @param
 * @param
 * @retval None
 * @example
 *
 */
void evm_app_resume();


/**
 * @brief   注册evue虚拟机的接口（要保证释放所有的内存和task）
 * @note
 * @param
 * @param
 * @param
 * @retval None
 * @example
 *
 */
void evm_app_regiter_destoy(evm_app_destory fn);

/**
 * @brief   退出evue虚拟机的接口（执行appfile中的onDestory函数， 释放所有的模块资源， 释放整个虚拟机内存）
 * @note
 * @param
 * @param
 * @param
 * @retval None
 * @example
 *
 */
void evm_app_stop();

/**
 * @brief  该函数用于启动evm应用， 执行appfile中的onCreate函数
 * @note
 * @param
 * @retval  0：成功 -1：失败
 */
int evm_create(const char* appfile);


#ifdef __cplusplus
}
#endif

#endif
