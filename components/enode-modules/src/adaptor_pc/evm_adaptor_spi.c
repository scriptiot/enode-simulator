#include "evm_module.h"


int evm_module_spi_open(int id, 
                        int mode, 
                        int freq, 
                        int polarity, 
                        int phase,
                        int firstbit){
    evm_print("evm_module_spi_open id:%d mode:%d freq:%d polarity%d phase:%d firstbit:%d\r\n",
                                    id, mode, freq, polarity, phase, firstbit);
    return 0;
}

int evm_module_spi_write(int id, const char *data, int len){
    evm_print("evm_module_spi_write id:%d len:%d\r\n", id, len);
    return 0;
}

int evm_module_spi_read(int id, char *data, int len){
    evm_print("evm_module_spi_read id:%d len:%d\r\n", id, len);
    return 0;
}

int evm_module_spi_transfer(int id, char *data_out, char *data_in, int len){
    evm_print("evm_module_spi_transfer id:%d len:%d\r\n", id, len);
    return 0;
}

int evm_module_spi_close(int id){
    evm_print("evm_module_spi_close id:%d\r\n", id);
    return 0;
}
