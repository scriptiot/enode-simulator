#include "evm_module.h"
#include <unistd.h>

int evm_adaptor_device_imei(char* imei, uint32_t len)
{
    static char *s_imei = "1234567890\0";
    int size = len > strlen(s_imei) ? strlen(s_imei) : len;
    memcpy(imei, s_imei, size+1);
    return 0;
}

static evm_device_t *s_dev_device = NULL;
static bool s_device_flag = false;

evm_device_t *evm_module_device_getInstance(void){
    return s_dev_device;
}

int evm_module_device_init(void){
    if(s_device_flag)
        return 0;

    s_dev_device = evm_malloc(sizeof (evm_device_t));
    if(s_dev_device == NULL)
        return -1;

    s_dev_device->imei = evm_adaptor_device_imei;

    s_device_flag = true;
    return 0;
}

void evm_module_device_destory(void){
    s_device_flag = false;
    if(s_dev_device != NULL)
        evm_free(s_dev_device);
    s_dev_device = NULL;
}
