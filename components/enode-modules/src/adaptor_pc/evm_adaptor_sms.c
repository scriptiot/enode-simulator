#include "evm_module.h"

int evm_adaptor_sms_send(const char* phonenum, const char* data){
    evm_print("evm_module_sms_send phonenum:%s data:%s\r\n", phonenum, data);
    return 0;
}

static evm_sms_t *s_dev_sms = NULL;
static bool s_sms_flag = false;
evm_sms_t *evm_module_sms_getInstance(void){
    return s_dev_sms;
}

int evm_module_sms_init(void){
    if(s_sms_flag)
        return 0;
    s_dev_sms = evm_malloc(sizeof (evm_sms_t));
    if(s_dev_sms == NULL)
        return -1;
    s_dev_sms->send = evm_adaptor_sms_send;

    s_sms_flag = true;
    return 0;
}
void evm_module_sms_destory(void){
    s_sms_flag = false;
    if(s_dev_sms != NULL)
        evm_free(s_dev_sms);
    s_dev_sms = NULL;
}
