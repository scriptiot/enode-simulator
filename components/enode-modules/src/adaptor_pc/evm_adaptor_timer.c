#include "evm_module.h"
#include "pthread.h"

static void * timer_callback(void *args)
{
//    evm_event_msg_t *msg = (evm_event_msg_t *)args;
//    int timeout = msg->param1 * 1000;
//    while(1)
//    {
//        usleep(timeout);

//        evm_event_msg_t *msgbuff = evm_adaptor_msg_create();
//        if(msgbuff == NULL)
//            break;

//        msgbuff->module = msg->module;
//        msgbuff->event = msg->event;
//        msgbuff->id = msg->id;

//        evm_adaptor_msg_put(msgbuff, 0);
//        if(strcmp(msg->event, "timeout")==0){
//            break;
//        }
//    }
    pthread_exit(NULL);
}
/* 定义线程pthread */
int evm_adaptor_timer_start(int id, void *param)
{
    pthread_t hd;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr,  PTHREAD_CREATE_DETACHED);
    if ((pthread_create(&hd, &attr, timer_callback, param)) == -1)
    {
        printf("create error!\n");
        return -1;
    }
}

int evm_adaptor_timer_stop(int id)
{
//    pthread_cancel();
}


static evm_timer_t *s_dev_timer = NULL;
static bool timer_init_flag = false;
evm_timer_t *evm_module_timer_getInstance(void){
    return s_dev_timer;
}

int evm_module_timer_init(void){
    if(timer_init_flag)
        return 0;
    s_dev_timer = evm_malloc(sizeof (evm_timer_t));
    if(s_dev_timer == NULL)
        return -1;
    s_dev_timer->start = evm_adaptor_timer_start;
    s_dev_timer->stop = evm_adaptor_timer_stop;

    timer_init_flag = true;
    return 0;
}

void evm_module_timer_destory(void){
    timer_init_flag = false;
    if(s_dev_timer != NULL)
        evm_free(s_dev_timer);
    s_dev_timer = NULL;
}
