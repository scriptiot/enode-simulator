#include "evm_module.h"
#include <unistd.h>

static char* s_os_platform = "win32";
char* evm_adaptor_os_platform(void){
    return s_os_platform;
}

static char* s_os_version = "v1.0.0";
char* evm_adaptor_os_version(void){
    return s_os_version;
}

int evm_adaptor_os_freemem(void){
    return 1024*1024*10;
}

int evm_adaptor_os_totalmem(void){
    return 1024*1024*32;
}

/**
 * @brief  evm_module_os_uptime
 * @note   获取时间戳（s）
 * @retval 运行秒数
 */
int evm_adaptor_os_uptime(void){
    return 123456789;
}

/**
 * @brief  evm_module_os_sleep
 * @note
 * @retval
 */
void evm_adaptor_os_delay(int ms){
    usleep(ms*1000);
}

static evm_os_t *s_dev_os = NULL;
static bool s_os_flag = false;

evm_os_t *evm_module_os_getInstance(void){
    return s_dev_os;
}

int evm_module_os_init(void){
    if(s_os_flag)
        return 0;

    s_dev_os = evm_malloc(sizeof (evm_os_t));
    if(s_dev_os == NULL)
        return -1;

    s_dev_os->platform = evm_adaptor_os_platform;
    s_dev_os->version = evm_adaptor_os_version;
    s_dev_os->freemem = evm_adaptor_os_freemem;
    s_dev_os->totalmem = evm_adaptor_os_totalmem;
    s_dev_os->uptime = evm_adaptor_os_uptime;
    s_dev_os->delay = evm_adaptor_os_delay;

    s_os_flag = true;
    return 0;
}

void evm_module_os_destory(void){
    if(s_dev_os != NULL)
        evm_free(s_dev_os);
    s_dev_os = NULL;
    s_os_flag = false;
}
