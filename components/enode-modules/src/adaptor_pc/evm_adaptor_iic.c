#include "evm_module.h"


int evm_module_iic_open(int id, int mode, int addr, int freq){
    evm_print("evm_module_iic_open id:%d mode:%d addr:%d freq:%d\r\n", id, mode, addr, freq);
    return 0;
}

int evm_module_iic_write(int id, int reg, const char *data, int len){
    evm_print("evm_module_iic_write id:%d len:%d\r\n", id, len);
    return 0;
}

int evm_module_iic_read(int id, int reg, char *data, int len){
    for(int i=0;i<len;i++)
        data[i] = i;
    evm_print("evm_module_iic_read id:%d len:%d\r\n", id, len);
    return 0;
}

int evm_module_iic_close(int id){
    evm_print("evm_module_iic_close id:%d\r\n", id);
    return 0;
}