#include "evm_module.h"


int evm_adaptor_adc_open(int id, int rate){
    evm_print("evm_module_adc_open id:%d rate:%d\r\n", id, rate);
    return 0;
}

int evm_adaptor_adc_read(int id){
    evm_print("evm_adaptor_adc_read id:%d\r\n", id);
    return 1234;
}

int evm_adaptor_adc_close(int id){
    evm_print("evm_adaptor_adc_close id:%d\r\n", id);
}

static evm_adc_t *s_dev_adc = NULL;
static bool s_adc_flag = false;
evm_adc_t *evm_module_adc_getInstance(void){
    return s_dev_adc;
}

int evm_module_adc_init(void){
    if(s_adc_flag)
        return 0;
    s_dev_adc = evm_malloc(sizeof (evm_adc_t));
    if(s_dev_adc == NULL)
        return -1;

    s_dev_adc->open = evm_adaptor_adc_open;
    s_dev_adc->read = evm_adaptor_adc_read;
    s_dev_adc->close = evm_adaptor_adc_close;

    s_adc_flag = true;
    return 0;
}
void evm_module_adc_destory(void){
    s_adc_flag = true;
    if(s_dev_adc != NULL)
        evm_free(s_dev_adc);
    s_dev_adc = NULL;
}
