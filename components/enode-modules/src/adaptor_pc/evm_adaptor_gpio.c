#include "evm_module.h"

static void * callback(void *args)
{
//    evm_event_msg_t *ptr = (evm_event_msg_t *)args;
//    evm_adaptor_msg_put(ptr, 0);
    return NULL;
}

//pin.setup(1,"in",gpio.PULLUP,{level})
int evm_adaptor_gpio_setup(int index, int dir, int pull, int level)
{
    evm_print("evm_module_pin_setup index:%d dir:%d pull:%d level:%d\r\n", index, dir, pull, level);
    return 0;
}
//pin.write(10,0)
int evm_adaptor_gpio_write(int index, int level)
{
    evm_print("evm_module_pin_write index:%d level:%d\r\n", index, level);
    return 0;
}
//pin.read(10)
int evm_adaptor_gpio_read(int index)
{
    evm_print("evm_module_pin_read index:%d\r\n", index);
    return 0;
}
//pin.toggle()
int evm_adaptor_gpio_toggle(int index){
    evm_print("evm_module_pin_toggle index:%d\r\n", index);
    return 0;
}
//pin.close()
void evm_adaptor_gpio_close(int index){
    evm_print("evm_module_pin_close index:%d\r\n", index);
}

int evm_adaptor_gpio_on(int index, void *param){
    evm_print("evm_adaptor_pin_on index:%d\r\n", index);
    return 0;
}

static evm_gpio_t *s_dev_gpio = NULL;
static bool s_gpio_flag = false;
int evm_module_gpio_init(void){
    if(s_gpio_flag)
        return 0;

    s_dev_gpio = evm_malloc(sizeof (evm_gpio_t));
    if(s_dev_gpio == NULL)
        return -1;

    s_dev_gpio->setup = evm_adaptor_gpio_setup;
    s_dev_gpio->read = evm_adaptor_gpio_read;
    s_dev_gpio->write = evm_adaptor_gpio_write;
    s_dev_gpio->toggle = evm_adaptor_gpio_toggle;
    s_dev_gpio->on = evm_adaptor_gpio_on;
    s_dev_gpio->close = evm_adaptor_gpio_close;

    s_gpio_flag = true;
    return 0;
}

evm_gpio_t *evm_module_gpio_getInstance(void){
    return s_dev_gpio;
}

void evm_module_pin_destory(void){
    s_gpio_flag = false;
    if(s_dev_gpio != NULL)
        evm_free(s_dev_gpio);
    s_dev_gpio = NULL;
}
