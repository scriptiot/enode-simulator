#include "evm_module.h"

int evm_adaptor_wdg_open(int timeout){
    evm_print("evm_module_wdg_open timeput:%d\r\n", timeout);
    return 0;
}

void evm_adaptor_wdg_feed(void){
    evm_print("evm_module_wdg_feed \r\n");
}

void evm_adaptor_wdg_close(void){
    evm_print("evm_module_wdg_close \r\n");
}

static evm_wdg_t *s_dev_wdg = NULL;
static bool s_wdg_flag = false;

evm_wdg_t *evm_module_wdg_getInstance(void){
    return s_dev_wdg;
}

int evm_module_wdg_init(void){
    if(s_wdg_flag)
        return 0;

    s_dev_wdg = evm_malloc(sizeof (evm_wdg_t));
    if(s_dev_wdg == NULL)
        return -1;
    s_dev_wdg->open = evm_adaptor_wdg_open;
    s_dev_wdg->feed = evm_adaptor_wdg_feed;
    s_dev_wdg->close = evm_adaptor_wdg_close;

    s_wdg_flag = true;
    return 0;
}

void evm_module_wgd_destory(void){
    s_wdg_flag = false;
    if(s_dev_wdg != NULL)
        evm_free(s_dev_wdg);
    s_dev_wdg = NULL;
}
