#include "evm_module_fs.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>

static bool s_fs_flag = false;
static evm_filesystem_t *fs = NULL;

int evm_module_fs_init(void){

    if(s_fs_flag == true)
        return 0;

    fs = evm_malloc(sizeof (evm_filesystem_t));
    if(fs == NULL)
        return -1;
    fs->open = open;
    fs->close = close;
    fs->read = read;
    fs->write = write;
    fs->access = access;
    fs->unlink = unlink;
    fs->tell = tell;
    fs->rename = rename;
    fs->remove = remove;
    fs->lseek = lseek;
    fs->getcwd = getcwd;
    fs->chdir = chdir;

    s_fs_flag = true;
    return 0;
}

evm_filesystem_t *evm_module_fs_getInstance(void){
    return fs;
}

void evm_module_fs_destory(void){
    s_fs_flag = false;
    if(fs!=NULL)
        evm_free(fs);
    fs = NULL;
}
