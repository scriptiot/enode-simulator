#include "evm_module.h"

int evm_adaptor_pwm_open(int id){
    evm_print("evm_adaptor_pwm_open channel:%d\r\n", id);
    return 0;
}

int evm_adaptor_pwm_setup(int id, int freq, int duty){
    evm_print("evm_adaptor_pwm_setup channel:%d freq:%d duty:%d\r\n", id, freq, duty);
    return 0;
}

int evm_adaptor_pwm_close(int id){
    evm_print("evm_adaptor_pwm_close id:%d\r\n", id);
    return 0;
}

static evm_pwm_t *s_dev_pwm = NULL;
static bool s_pwm_flag = false;

evm_pwm_t *evm_module_pwm_getInstance(void){
    return s_dev_pwm;
}

int evm_module_pwm_init(void){
    if(s_pwm_flag)
        return 0;
    s_dev_pwm = evm_malloc(sizeof(evm_pwm_t));
    if(s_dev_pwm == NULL)
        return -1;

    s_dev_pwm->open = evm_adaptor_pwm_open;
    s_dev_pwm->setup = evm_adaptor_pwm_setup;
    s_dev_pwm->close = evm_adaptor_pwm_close;

    s_pwm_flag = true;
    return 0;
}
void evm_module_pwm_destory(void){
    s_pwm_flag = false;
    if(s_dev_pwm!=NULL){
        evm_free(s_dev_pwm);
    }
    s_dev_pwm = NULL;
}
