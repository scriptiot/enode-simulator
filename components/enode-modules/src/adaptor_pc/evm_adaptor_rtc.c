#include "evm_module.h"



int evm_adaptor_rtc_open(void){
    evm_print("evm_adaptor_rtc_open \r\n");
    return 0;
}

int evm_adaptor_rtc_setTime(int year, int month, int date, int hours, int minutes, int seconds){
    evm_print("evm_adaptor_rtc_setTime year:%d month:%d date:%d hours:%d minutes:%d seconds:%d\r\n",
              year, month, date, hours, minutes, seconds);
    return 0;
}

int evm_adaptor_rtc_getTime(void *ptr){
    rtc_date_t *date = (rtc_date_t *)ptr;
    date->year = 2021;
    date->month = 11;
    date->date = 2;
    date->hours = 13;
    date->minutes = 53;
    date->seconds = 0;
    evm_print("evm_adaptor_rtc_getTime \r\n");
    return 0;
}

int evm_adaptor_rtc_close(void){
    evm_print("evm_adaptor_rtc_close \r\n");
    return 0;
}

static evm_rtc_t *s_dev_rtc = NULL;
static bool s_rtc_flag = false;

evm_rtc_t *evm_module_rtc_getInstance(void){
    return s_dev_rtc;
}

int evm_module_rtc_init(void){
    if(s_rtc_flag)
        return 0;
    s_dev_rtc = evm_malloc(sizeof(evm_rtc_t));
    if(s_dev_rtc == NULL)
        return -1;

    s_dev_rtc->open = evm_adaptor_rtc_open;
    s_dev_rtc->setTime = evm_adaptor_rtc_setTime;
    s_dev_rtc->getTime = evm_adaptor_rtc_getTime;
    s_dev_rtc->close = evm_adaptor_rtc_close;

    s_rtc_flag = true;
    return 0;
}
void evm_module_rtc_destory(void){
    s_rtc_flag = false;
    if(s_dev_rtc != NULL)
        evm_free(s_dev_rtc);
    s_dev_rtc = NULL;
}
