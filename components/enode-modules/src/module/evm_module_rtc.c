#include "evm_module.h"

void* evm_module_rtc_createDate(void){
    return evm_malloc(sizeof(rtc_date_t));
}

void evm_module_rtc_destoryDate(void *date){
    evm_free(date);
}

int evm_module_rtc_getFullYear(void *date){
    if(date == NULL)
        return -1;
    return ((rtc_date_t*)date)->year;
}

int evm_module_rtc_getMonth(void *date){
    if(date == NULL)
        return -1;
    return ((rtc_date_t*)date)->month;
}

int evm_module_rtc_getDate(void *date){
    if(date == NULL)
        return -1;
    return ((rtc_date_t*)date)->date;
}

int evm_module_rtc_getHours(void *date){
    if(date == NULL)
        return -1;
    return ((rtc_date_t*)date)->hours;
}

int evm_module_rtc_getMinutes(void *date){
    if(date == NULL)
        return -1;
    return ((rtc_date_t*)date)->minutes;
}

int evm_module_rtc_getSeconds(void *date){
    if(date == NULL)
        return -1;
    return ((rtc_date_t*)date)->seconds;
}

evm_err_t evm_module_rtc(evm_t *e) {
    if(evm_module_rtc_init()!=0)
        return ec_err;
    evm_rtc_t *rtc = evm_module_rtc_getInstance();
    if(rtc == NULL)
        return ec_err;

    evm_cffi_t funcs[] = {
        {"open", (intptr_t)rtc->open, "i"},
        {"setTime", (intptr_t)rtc->setTime, "iiiiiii"},
        {"getTime", (intptr_t)rtc->getTime, "ip"},
        {"close", (intptr_t)rtc->close, "i"},
        {"getFullYear", (intptr_t)evm_module_rtc_getFullYear, "ip"},
        {"getMonth", (intptr_t)evm_module_rtc_getMonth, "ip"},
        {"getDate", (intptr_t)evm_module_rtc_getDate, "ip"},
        {"getHours", (intptr_t)evm_module_rtc_getHours, "ip"},
        {"getMinutes", (intptr_t)evm_module_rtc_getMinutes, "ip"},
        {"getSeconds", (intptr_t)evm_module_rtc_getSeconds, "ip"},
        {"createDate", (intptr_t)evm_module_rtc_createDate, "p"},
        {"destoryDate", (intptr_t)evm_module_rtc_destoryDate, "vp"},

        {NULL, 0, NULL}
    };

    evm_builtin_t builtin[] = {
        {NULL, EVM_VAL_UNDEFINED}
    };
    evm_module_effi_create(e, "@native.rtc", funcs , builtin);
    return e->err;
}
