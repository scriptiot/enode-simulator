#include "evm_module.h"

evm_err_t evm_module_wdg(evm_t *e) {
    if(evm_module_wdg_init() != 0)
        return ec_err;

    evm_wdg_t *wdg = evm_module_wdg_getInstance();
    if(wdg == NULL)
        return ec_err;

    evm_cffi_t funcs[] = {
        {"open", (intptr_t)wdg->open, "ii"},
        {"feed", (intptr_t)wdg->feed, "v"},
        {"close", (intptr_t)wdg->close, "v"},
        {NULL, 0, NULL}
    };

    evm_builtin_t builtin[] = {
        {NULL, EVM_VAL_UNDEFINED}
    };
    evm_module_effi_create(e, "@native.wdg", funcs , builtin);
    return e->err;
}
