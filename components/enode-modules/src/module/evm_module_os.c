#include "evm_module.h"

evm_err_t evm_module_os(evm_t *e) {
    if(evm_module_os_init() != 0)
        return ec_err;
    evm_os_t *os = evm_module_os_getInstance();
    if(os == NULL)
        return ec_err;

    evm_cffi_t funcs[] = {
        {"platform", (intptr_t)os->platform, "s"},
        {"version", (intptr_t)os->version, "s"},
        {"freemem", (intptr_t)os->freemem, "i"},
        {"totalmem", (intptr_t)os->totalmem, "i"},
        {"uptime", (intptr_t)os->uptime, "i"},
        {"delay", (intptr_t)os->delay, "vi"},
        {NULL, 0, NULL}
    };

    evm_builtin_t builtin[] = {
        {NULL, EVM_VAL_UNDEFINED}
    };
    evm_module_effi_create(e, "@system.os", funcs , builtin);
    return e->err;
}

