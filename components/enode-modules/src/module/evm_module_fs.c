#include "evm.h"
#include "evm_module_cffi.h"
#include "evm_module_fs.h"


evm_err_t evm_module_fs(evm_t *e) {
    if(evm_module_fs_init()!=0)
        return ec_err;
    evm_filesystem_t *fs = evm_module_fs_getInstance();
    if(fs == NULL)
        return ec_err;

     evm_cffi_t funcs[] = {
        {"open", (uintptr_t)fs->open, "isii"},
        {"close", (uintptr_t)fs->close, "ii"},
        {"read", (uintptr_t)fs->read, "iipi"},
        {"write", (uintptr_t)fs->write, "iipi"},
        {"lseek", (uintptr_t)fs->lseek, "iiii"},
        {"rename", (uintptr_t)fs->rename, "iss"},
        {"unlink", (uintptr_t)fs->unlink, "is"},
        {"remove", (uintptr_t)fs->remove, "is"},
        {"tell", (uintptr_t)fs->tell, "ii"},
        {"access", (uintptr_t)fs->access, "isi"},
        {"getcwd", (uintptr_t)fs->getcwd, "spi"},
        {"chdir", (uintptr_t)fs->chdir, "is"},
        {NULL, 0, NULL}
    };

    evm_builtin_t builtin[] = {
        {NULL, EVM_VAL_UNDEFINED}
    };
    evm_module_effi_create(e, "@native.fs", funcs , builtin);
    return e->err;
}


