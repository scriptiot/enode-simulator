#include "evm_module.h"

evm_err_t evm_module_pwm(evm_t *e) {
    if(evm_module_pwm_init() != 0)
        return ec_err;
    evm_pwm_t *pwm = evm_module_pwm_getInstance();
    if(pwm == NULL)
        return ec_err;

    evm_cffi_t funcs[] = {
        {"open", (intptr_t)pwm->open, "ii"},
        {"setup", (intptr_t)pwm->setup, "iiii"},
        {"close", (intptr_t)pwm->close, "ii"},
        {NULL, 0, NULL}
    };

    evm_builtin_t builtin[] = {
        {NULL, EVM_VAL_UNDEFINED}
    };
    evm_module_effi_create(e, "@native.pwm", funcs , builtin);
    return e->err;
}

