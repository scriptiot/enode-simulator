#include "evm_module.h"


evm_err_t evm_module_device(evm_t *e){
    if(evm_module_device_init()!=0)
        return ec_err;
    evm_device_t *device = evm_module_device_getInstance();
    if(device == NULL)
        return ec_err;

    evm_cffi_t funcs[] = {
        {"imei", (uintptr_t)device->imei, "ipi"},
        {NULL, 0, NULL}
    };

    evm_builtin_t builtin[] = {
        {NULL, EVM_VAL_UNDEFINED}
    };
    evm_module_effi_create(e, "@native.device", funcs , builtin);
    return e->err;
}
