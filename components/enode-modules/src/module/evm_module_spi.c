#include "evm_module.h"

evm_err_t evm_module_spi(evm_t *e) {
    evm_cffi_t funcs[] = {
        {"open", (uintptr_t)evm_module_spi_open, "iiiiiii"},
        {"write", (uintptr_t)evm_module_spi_write, "iipi"},
        {"read", (uintptr_t)evm_module_spi_read, "iipi"},
        {"transfer", (uintptr_t)evm_module_spi_transfer, "iippi"},
        {"close", (uintptr_t)evm_module_spi_close, "ii"},
        {NULL, 0, NULL}
    };

    evm_builtin_t builtin[] = {
        {NULL, EVM_VAL_UNDEFINED}
    };
    evm_module_effi_create(e, "@native.spi", funcs , builtin);
    return e->err;
}

