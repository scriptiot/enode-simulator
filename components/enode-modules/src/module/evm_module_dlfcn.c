#include <dlfcn.h>
#include "evm.h"
#include "evm_module_cffi.h"
#include <stdarg.h>

evm_err_t evm_module_dlfcn_destory(evm_t *e){

}

evm_err_t evm_module_dlfcn(evm_t *e) {
    evm_cffi_t funcs[] = {
        {"dlopen", (uintptr_t)dlopen, "psi"},
        {"dlclose", (uintptr_t)dlclose, "ip"},
        {"dlsym", (uintptr_t)dlsym, "pps"},
        {"dlerror", (uintptr_t)dlerror, "s"},
//        {"dladdr", (uintptr_t)dladdr, "ipp"},
        {NULL, 0, NULL}
    };
    evm_module_effi_create(e, "@system.dlfcn", funcs , NULL);
    return e->err;
}
