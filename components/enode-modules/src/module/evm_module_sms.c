#include "evm_module.h"

evm_err_t evm_module_sms(evm_t *e) {
    if(evm_module_sms_init() != 0)
        return ec_err;
    evm_sms_t *sms = evm_module_sms_getInstance();
    if(sms == NULL)
        return  ec_err;

    evm_cffi_t funcs[] = {
        {"send", (intptr_t)sms->send, "iss"},
        {NULL, 0, NULL}
    };

    evm_builtin_t builtin[] = {
        {NULL, EVM_VAL_UNDEFINED}
    };
    evm_module_effi_create(e, "@system.sms", funcs , builtin);
    return e->err;
}

