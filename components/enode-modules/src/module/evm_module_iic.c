#include "evm_module.h"

evm_err_t evm_module_iic(evm_t *e) {
    static evm_cffi_t funcs[] = {
        {"open", (uintptr_t)evm_module_iic_open, "iiiii"},
        {"write", (uintptr_t)evm_module_iic_write, "iiipi"},
        {"read", (uintptr_t)evm_module_iic_read, "iiipi"},
        {"close", (uintptr_t)evm_module_iic_close, "ii"},
        {NULL, 0, NULL}
    };

    evm_builtin_t builtin[] = {
        {NULL, EVM_VAL_UNDEFINED}
    };
    evm_module_effi_create(e, "@native.iic", funcs , builtin);
    return e->err;
}

