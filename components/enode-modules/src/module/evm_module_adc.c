#include "evm_module.h"

evm_err_t evm_module_adc(evm_t *e) {
    if(evm_module_adc_init() != 0)
        return ec_err;

    evm_adc_t *adc = evm_module_adc_getInstance();
    if(adc == NULL)
        return ec_err;

    evm_cffi_t funcs[] = {
        {"open", (intptr_t)adc->open, "iii"},
        {"read", (intptr_t)adc->read, "ii"},
        {"close", (intptr_t)adc->close, "vi"},
        {NULL, 0, NULL}
    };

    evm_builtin_t builtin[] = {
        {NULL, EVM_VAL_UNDEFINED}
    };
    evm_module_effi_create(e, "@native.adc", funcs , builtin);
    return e->err;
}

