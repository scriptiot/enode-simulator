#ifndef __EVM_MODULE_ADC_H
#define __EVM_MODULE_ADC_H
#include "evm_module.h"


/************************ ADC ****************************/

typedef struct evm_adc{
    /**
     * @brief  int open(int id, int rate)
     * @note   打开ADC指定通道
     * @param  id: 通道
     * @param  rate: 采样率
     * @retval =0：成功
     *         <0：失败
     */
    int (*open)(int, int);

    /**
     * @brief  int close(int id)
     * @note   关闭ADC指定通道
     * @param  id: 通道
     * @retval =0：成功
     *         <0：失败
     */
    int (*close)(int);
    /**
     * @brief  int read(int id)
     * @note   读取ADC指定通道采样值
     * @param  id: 通道
     * @retval >=0 采样值
     *         < 0 读取失败
     */
    int (*read)(int);
}evm_adc_t;

evm_adc_t *evm_module_adc_getInstance(void);

int evm_module_adc_init(void);
void evm_module_adc_destory(void);

evm_err_t evm_module_adc(evm_t *e);
#endif

