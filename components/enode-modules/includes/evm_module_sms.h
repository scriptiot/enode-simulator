#ifndef __EVM_MODULE_SMS_H
#define __EVM_MODULE_SMS_H
#include "evm_module.h"

/************************* SMS *****************************/
typedef struct evm_sms{
    /**
     * @brief  int send(const char *phonenum, const char *data)
     * @note   发送短信
     * @param  phonenum:手机号
     * @param  data: 短信内容
     * @retval =0：成功
     *         <0：失败
     */
    int (*send)(const char*, const char *);
}evm_sms_t;

evm_sms_t *evm_module_sms_getInstance(void);

int evm_module_sms_init(void);
void evm_module_sms_destory(void);

evm_err_t evm_module_sms(evm_t *e);
#endif

