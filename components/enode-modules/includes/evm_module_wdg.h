#ifndef __EVM_MODULE_WDG_H
#define __EVM_MODULE_WDG_H
#include "evm_module.h"


/************************ WDG ****************************/
typedef struct evm_wdg{
    /**
     * @brief  int open(int timeout)
     * @note   配置看门狗
     * @param  timeout: 看门狗超时时间
     * @retval
     */
    int (*open)(int);

    /**
     * @brief  int feed(void)
     * @note   喂狗
     * @retval
     */
    void (*feed)(void);

    /**
     * @brief  int close(void);
     * @note   关闭看门狗
     * @retval
     */
    void (*close)(void);
}evm_wdg_t;

evm_wdg_t *evm_module_wdg_getInstance(void);

int evm_module_wdg_init(void);

void evm_module_wgd_destory(void);

evm_err_t evm_module_wdg(evm_t *e);
#endif

