#ifndef __EVM_MODULE_TCP_H
#define __EVM_MODULE_TCP_H
#include "evm_module.h"


typedef struct tcp_callback {
    char *type;
    int error;
    msg_buffer_t *buffer;
}tcp_callback_t;

typedef struct evm_tcp{
    /**
     * @brief  int connect(const char* ip, int port)
     * @note   连接服务器
     * @param  *ip: 服务器IP
     * @param  port: 服务器端口
     * @param  family:
     * @retval >=0：文件描述符
     *         < 0：失败
     */
    int (*connect)(const char*, int);

    /**
     * @brief  int send(int fd, const char* data, int len)
     * @note   发送数据
     * @param  fd: 文件描述符
     * @param  *data: 数据
     * @param  len: 长度
     * @retval
     */
    int (*send)(int, const char*, int);

    /**
     * @brief  int on(int fd, void *param)
     * @note   注册回调事件
     * @param  fd: 文件描述符
     * @param  param: 参数
     * @retval
     */
    int (*on)(int, void *);

    /**
     * @brief  int close(int fd)
     * @note   关闭TCP连接
     * @param  fd: 文件描述符
     * @retval
     */
    int (*close)(int);
}evm_tcp_t;


evm_tcp_t *evm_module_tcp_getInstance(void);

int evm_module_tcp_init(void);
void evm_module_tcp_destory(void);

evm_err_t evm_module_tcp(evm_t *e);
#endif

