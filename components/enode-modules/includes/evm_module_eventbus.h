#ifndef __EVM_MODULE_EVENTBUS_H
#define __EVM_MODULE_EVENTBUS_H
#include "evm.h"
#include "evm_module_cffi.h"

typedef evm_val_t event_cb(evm_t *e, void *msg);
typedef void event_data_free(void *data);

typedef struct evm_event_msg{
    int id;
    char *event;
    void *data;
    event_cb *cb;
    event_data_free *free;
}evm_event_msg_t;

// /**
//  * @brief  evm_adaptor_msg_init
//  * @note   初始化消息队列
//  * @retval =0：成功 
//  *         <0：失败
//  */
// void* evm_adaptor_msgqueue_create(void);
// /**
//  * @brief  evm_adaptor_msg_put
//  * @note   发送消息
//  * @param  *ptr: 消息指针
//  * @param  timeout: 超时时间
//  * @retval 
//  */
// int evm_adaptor_msg_put(const evm_event_msg_t *ptr, int timeout);
// /**
//  * @brief  evm_adaptor_msg_get
//  * @note   接收消息
//  * @param  ptr: 数据缓存区
//  * @param  timeout: 超时时间
//  * @retval =0：成功 
//  *         <0：失败
//  */
// int evm_adaptor_msg_get(evm_event_msg_t* ptr, int timeout);
// /**
//  * @brief  evm_adaptor_msg_free
//  * @note   释放消息
//  * @retval 
//  */
// void evm_adaptor_msg_clear(evm_event_msg_t* msg);

// /**
//  * @brief evm_adaptor_msg_create
//  * @return 消息指针
//  */
// evm_event_msg_t *evm_adaptor_msg_create(void);
// /**
//  * @brief  evm_adaptor_msg_destory
//  * @note   清空消息
//  * @retval
//  */
// void evm_adaptor_msg_destory(void);

void* evm_module_msgqueue_create(int size, int len);

int evm_module_msgqueue_destory(void *queue);

int evm_module_msg_put(const evm_event_msg_t *ptr, int timeout);

int evm_module_msg_get(evm_event_msg_t *ptr, int timeout);

evm_event_msg_t *evm_module_msg_create(void);

void evm_module_msg_clear(evm_event_msg_t *msg);

void evm_module_msg_destory(evm_event_msg_t *msg);

int evm_module_eventbus_init(void);

void evm_module_eventbus_destory(void);

void evm_eventbus_poll(evm_t *e);

int evm_module_eventbus(evm_t *e);
#endif

