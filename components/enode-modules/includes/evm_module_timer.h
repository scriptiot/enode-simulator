#ifndef __EVM_MODULE_TIMER_H
#define __EVM_MODULE_TIMER_H
#include "evm_module.h"


/************************ TIMER **************************/
///**
// * @brief  evm_module_timer_start
// * @note   启动定时器
// * @param  id: 定时器ID
// * @param  *param: 回调函数参数
// * @retval >=0 分配ID
// *         < 0 失败
// */
//int evm_adaptor_timer_start(int id, void *param);
///**
// * @brief  evm_module_timer_stop
// * @note   关闭定时器
// * @param  id: 定时器ID
// * @retval
// */
//int evm_adaptor_timer_stop(int id);


typedef struct evm_timer{
    /**
     * @brief  int start(int id, void* param)
     * @note   启动定时器
     * @param  id: 定时器ID
     * @param  *param: 回调函数参数
     * @retval >=0 分配ID
     *         < 0 失败
     */
    int (*start)(int, void*);

    /**
     * @brief  int stop(int id)
     * @note   关闭定时器
     * @param  id: 定时器ID
     * @retval
     */
    int (*stop)(int);
}evm_timer_t;


evm_timer_t *evm_module_timer_getInstance(void);

int evm_module_timer_init(void);

void evm_module_timer_destory(void);

evm_err_t evm_module_timer(evm_t *e);
#endif

