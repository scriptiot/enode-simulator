#ifndef __EVM_MODULE_UART_H
#define __EVM_MODULE_UART_H
#include "evm_module.h"

/************************ UART ****************************/
////uart.open(id, baud, databits, stopbits, parity);
///**
// * @brief  evm_module_uart_open
// * @note   打开串口
// * @param  id: 串口号
// * @param  baud: 波特率
// * @param  databits: 数据位
// * @param  stopbits: 停止位
// * @param  parity: 校验位
// * * @retval =0：成功
// *         <0：失败
// */
//int evm_module_uart_open(int id, int baud, int databits, int stopbits, int parity, int buffersize);
////uart.send(data, len)
///**
// * @brief  evm_module_uart_write
// * @note   串口发送数据
// * @param  id：串口号
// * @param  *data: 数据
// * @param  len: 长度
// * @retval =0：成功
// *         <0：失败
// */
//int evm_module_uart_write(int id, char *data, int len);
///**
// * @brief  evm_module_uart_read
// * @note   串口读取数据
// * @param  id：串口号
// * @param  *data: 数据
// * @param  len: 长度
// * @retval >=0：成功
// *         < 0：失败
// */
//int evm_module_uart_read(int id, char *data, int len, int timeout);
///**
// * @brief  evm_module_uart_size
// * @note   获取接收缓存区数据长度
// * @retval 数据长度
// */
//int evm_module_uart_size(void);
///**
// * @brief  evm_adaptor_uart_on
// * @note   串口注册回调
// * @param  id: 串口ID
// * @param  *param: 回调函数参数
// * @retval >=0 成功
// *         < 0 失败
// */
//int evm_adaptor_uart_on(int id, void *param);

////uart.close(id)
///**
// * @brief  evm_module_uart_close
// * @note   关闭串口
// * @param  id: 串口号
// * @retval
// */
//int evm_module_uart_close(int id);


typedef struct evm_uart{
    /**
     * @brief  int open(int id, int baud, int databits, int stopbits, int parity, int buffersize)
     * @note   打开串口
     * @param  id: 串口号
     * @param  baud: 波特率
     * @param  databits: 数据位
     * @param  stopbits: 停止位
     * @param  parity: 校验位
     * @param  buffersize: 缓存区大小
     * @retval =0：成功
     *         <0：失败
     */
    int (*open)(int, int, int, int, int, int);

    /**
     * @brief  int read(int id, char *data, int len)
     * @note   串口读取数据
     * @param  id：串口号
     * @param  *data: 数据
     * @param  len: 长度
     * @retval =0：成功
     *         <0：失败
     */
    int (*read)(int, char*, int, int);

    /**
     * @brief  int write(int id, char *data, int len);
     * @note   串口发送数据
     * @param  id：串口号
     * @param  *data: 数据
     * @param  len: 长度
     * @retval =0：成功
     *         <0：失败
     */
    int (*write)(int, const char*, int);

    /**
     * @brief  int on(int id, void* cb, void *param)
     * @note   串口注册回调
     * @param  id: 串口ID
     * @param  *cb: 回调函数
     * @param  *param: 回调函数参数
     * @retval >=0 成功
     *         < 0 失败
     */
    int (*on)(int, void*);

    /**
     * @brief  int evm_module_uart_close(int id);
     * @note   关闭串口
     * @param  id: 串口号
     * @retval
     */
    int (*close)(int);
}evm_uart_t;

evm_uart_t *evm_module_uart_getInstance(void);

/**
 * @brief  evm_module_uart_init
 * @note   UART模块初始化
 * @retval = 0 成功
 *         < 0 失败
 */
int evm_module_uart_init(void);

/**
 * @brief  evm_module_uart_destory
 * @note   UART模块销毁
 * @retval None
 */
void evm_module_uart_destory(void);

evm_err_t evm_module_uart(evm_t *e);
#endif

