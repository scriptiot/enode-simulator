#ifndef __EVM_MODULE_OS_H
#define __EVM_MODULE_OS_H
#include "evm_module.h"

typedef struct evm_os{
    /**
     * @brief  char *platform(void)
     * @note   获取平台类型
     * @retval 平台
     */
    char* (*platform)(void);

    /**
     * @brief  char *version(void)
     * @note   获取版本
     * @retval 版本
     */
    char* (*version)(void);

    /**
     * @brief  int freemem(void)
     * @note   获取剩余内存
     * @retval 剩余内存
     */
    int (*freemem)(void);

    /**
     * @brief  int totalmem
     * @note   获取全部内存
     * @retval 全部内存
     */
    int (*totalmem)(void);

    /**
     * @brief  int uptime(void)
     * @note   获取时间戳
     * @retval 运行时间
     */
    int (*uptime)(void);


    /**
     * @brief  void delay(int ms)
     * @note   延时毫秒
     * @retval
     */
    void (*delay)(int);
}evm_os_t;

evm_os_t *evm_module_os_getInstance(void);

int evm_module_os_init(void);
void evm_module_os_destory(void);


evm_err_t evm_module_os(evm_t *e);
#endif

