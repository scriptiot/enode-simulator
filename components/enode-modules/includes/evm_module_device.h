#ifndef __EVM_MODULE_DEVICE_H
#define __EVM_MODULE_DEVICE_H
#include "evm_module.h"

/************************ DEVICE ****************************/
typedef struct evm_device{
    /**
     * @brief  int imei(char *addr, int len)
     * @note   获取IMEI
     * @param  addr: 数据地址
     * @param  len: 数据长度
     * @retval =0：成功
     *         <0：失败
     */
    int (*imei)(char*, size_t);
}evm_device_t;

evm_device_t *evm_module_device_getInstance(void);

int evm_module_device_init(void);
void evm_module_device_destory(void);

evm_err_t evm_module_device(evm_t *e);
#endif

