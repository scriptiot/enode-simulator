#ifndef __EVM_MODULE_IIC_H
#define __EVM_MODULE_IIC_H
#include "evm_module.h"


/************************ IIC ****************************/
//iic.open(id, mode, addr, freq) 
/**
 * @brief  evm_module_iic_open
 * @note   配置IIC
 * @param  id: IIC ID
 * @param  mode: 工作模式
 * @param  addr: 地址
 * @param  freq: 频率
 * @retval 
 */
int evm_module_iic_open(int id, int mode, int addr, int freq);
//iic.write(id, reg, data, len)
/**
 * @brief  evm_module_iic_write
 * @note   发送数据
 * @param  id: IIC ID
 * @param  reg: 寄存器地址
 * @param  *data: 待发送数据
 * @param  len: 数据长度
 * @retval 
 */
int evm_module_iic_write(int id, int reg, const char *data, int len);
//iic.read(id, reg, data, len)
/**
 * @brief  evm_module_iic_read
 * @note   读取数据
 * @param  id: IIC ID
 * @param  reg: 寄存器地址
 * @param  *data: 数据缓存区
 * @param  len: 读取字节数
 * @retval 
 */
int evm_module_iic_read(int id, int reg, char *data, int len);
//iic_close(id)
/**
 * @brief  evm_module_iic_close
 * @note   关闭IIC
 * @param  id: IIC ID
 * @retval 
 */
int evm_module_iic_close(int id);

evm_err_t evm_module_iic(evm_t *e);
#endif

