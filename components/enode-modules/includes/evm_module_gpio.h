#ifndef __EVM_MODULE_PIN_H
#define __EVM_MODULE_PIN_H
#include "evm_module.h"


/************************ PIN ****************************/
typedef struct evm_gpio{

    /**
     * @brief  int setup(int index, int dir, int pull, int level)
     * @note   引脚配置
     * @param  index: 引脚号
     * @param  dir: 方向
     * @param  pull: 上下拉
     * @param  level: 电平状态
     * @retval 结果
     */
    int (*setup)(int, int, int, int);

    /**
     * @brief  int write(int index, int level)
     * @note   设置引脚电平
     * @param  index: 引脚号
     * @param  level: 电平
     * @retval
     */
    int (*write)(int, int);

    /**
     * @brief  int read(int index)
     * @note   读取引脚电平
     * @param  index:
     * @retval
     */
    int (*read)(int);

    /**
     * @brief  int toggle(int index)
     * @note   引脚状态翻转
     * @param  index: 引脚号
     * @retval
     */
    int (*toggle)(int);

    /**
     * @brief  int on(int index, void *param)
     * @note   注册回调事件
     * @param  index: 引脚号
     * @param  *param: 参数
     * @retval
     */
    int (*on)(int, void*);

    /**
     * @brief  int close(int index)
     * @note   关闭引脚
     * @param  index: 引脚号
     * @retval
     */
    int (*close)(int);
}evm_gpio_t;

evm_gpio_t *evm_module_gpio_getInstance(void);

/**
 * @brief evm_module_gpio_init
 * @note  GPIO模块初始化
 * @return
 */
int evm_module_gpio_init(void);

/**
 * @brief evm_module_gpio_destory
 */
void evm_module_gpio_destory(void);

evm_err_t evm_module_gpio(evm_t *e);
#endif

