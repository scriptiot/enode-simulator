#ifndef __EVM_MODULE_SPI_H
#define __EVM_MODULE_SPI_H
#include "evm_module.h"

/************************ SPI ****************************/
//spi.open(id, mode, freq, width, firstbit) 
/**
 * @brief  evm_module_spi_open
 * @note   配置SPI
 * @param  id: SPI ID 
 * @param  mode: 工作模式
 * @param  freq: 工作频率
 * @param  polarity: 极性
 * @param  phase: 相位
 * @param  firstbit: 大小端
 * @retval 
 */
int evm_module_spi_open(int id, 
                        int mode, 
                        int freq, 
                        int polarity, 
                        int phase,
                        int firstbit);
//spi.write(id, data, len)
/**
 * @brief  evm_module_spi_write
 * @note   发送数据
 * @param  id: SPI ID 
 * @param  *data: 待发送数据
 * @param  len: 数据长度
 * @retval 
 */
int evm_module_spi_write(int id, const char *data, int len);
//spi.read(id, data, len)
/**
 * @brief  evm_module_spi_read
 * @note   读取数据
 * @param  id: SPI ID 
 * @param  *data: 数据缓存区
 * @param  len: 待读取长度
 * @retval 
 */
int evm_module_spi_read(int id, char *data, int len);
//spi.transfer(id, out, in, len)
/**
 * @brief  evm_module_spi_transfer
 * @note   
 * @param  id: SPI ID 
 * @param  *data_out: 待发送数据
 * @param  *data_in: 数据缓存区
 * @param  len: 数据长度
 * @retval 
 */
int evm_module_spi_transfer(int id, char *data_out, char *data_in, int len);
//spi.close(id)
/**
 * @brief  evm_module_spi_close
 * @note   关闭SPI
 * @param  id: SPI ID 
 * @retval 
 */
int evm_module_spi_close(int id);

evm_err_t evm_module_spi(evm_t *e);
#endif

