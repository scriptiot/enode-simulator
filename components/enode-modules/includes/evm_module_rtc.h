#ifndef __EVM_MODULE_RTC_H
#define __EVM_MODULE_RTC_H
#include "evm_module.h"


/************************ RTC ****************************/
typedef struct rtc_date{
    int year;
    int month;
    int date;
    int hours;
    int minutes;
    int seconds;
}rtc_date_t;


typedef struct evm_rtc{
    /**
     * @brief  int open()
     * @note   打开RTC
     * @retval =0：成功
     *         <0：失败
     */
    int (*open)(void);

    /**
     * @brief  int setTime(int year, int month, int date, int hours, int minutes, int seconds)
     * @note   设置时间
     * @param  year: 年
     * @param  month: 月
     * @param  date: 日
     * @param  hours: 时
     * @param  minutes: 分
     * @param  seconds: 秒
     * @retval =0：成功
     *         <0：失败
     */
    int (*setTime)(int, int, int, int, int, int);

    /**
     * @brief  int getTime(rtc_date_t * date)
     * @note   读取时间
     * @param  *ptr: DateStruct地址
     * @retval =0：成功
     *         <0：失败
     */
    int (*getTime)(rtc_date_t *);

    /**
     * @brief  int close()
     * @note   关闭RTC
     * @retval
     */
    int (*close)(void);
}evm_rtc_t;

evm_rtc_t *evm_module_rtc_getInstance(void);

int evm_module_rtc_init(void);
void evm_module_rtc_destory(void);

evm_err_t evm_module_rtc(evm_t *e);
#endif

