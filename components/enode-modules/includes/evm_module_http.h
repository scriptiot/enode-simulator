#ifndef __EVM_MODULE_HTTP_H
#define __EVM_MODULE_HTTP_H
#include "evm_module.h"


/************************ HTTP ****************************/

#define WEBCLIENT_HEADER_BUFSZ         10240
#define WEBCLIENT_RESPONSE_BUFSZ       10240

typedef struct http_response{
    int code;
    char *responseType;
    msg_buffer_t *data;
    msg_buffer_t *header;
}http_response_t;


/**
 * @brief  evm_adaptor_http_request
 * @note   HTTP请求
 * @param  method:          请求方式
 * @param  url:             地址
 * @param  header:          请求头
 * @param  body:            请求数据
 * @param  responseType:    响应类型
 * @param  timeout:         超时时间
 * @param  msg:             数据
 * @retval 
 */
int evm_adaptor_http_request(const char* method, 
                            const char* url, 
                            const char* header, 
                            const char* body, 
                            const char* responseType,
                            int timeout, 
                            void *msg);

int evm_module_http_init(void);

void evm_module_http_destory(void);

evm_err_t evm_module_http(evm_t *e);
#endif

