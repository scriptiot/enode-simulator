#ifndef __EVM_MODULE_FS_H
#define __EVM_MODULE_FS_H

#include "evm_module.h"

typedef struct evm_filesystem{
    int (*open)(const char *, int,...);
    int (*close)(int);
    size_t (*read)(int, void*, size_t);
    size_t (*write)(int, const void*, size_t);
    int (*access)(const char *, int);
    int (*unlink)(const char *);
    int (*tell)(int);
    int (*rename)(const char *, const char *);
    int (*remove)(const char *);
    int (*lseek)(int, int, int);
    char* (*getcwd)(char *, int);
    int (*chdir)(const char *);
}evm_filesystem_t;

evm_filesystem_t *evm_module_fs_getInstance(void);

int evm_module_fs_init(void);
void evm_module_fs_destory(void);

evm_err_t evm_module_fs(evm_t *e);
#endif

