#ifndef __EVM_MODULE_BUILTIN_H
#define __EVM_MODULE_BUILTIN_H
#include "evm_module.h"


/************************ SYS ****************************/
evm_err_t evm_module_builtin(evm_t *e);
#endif

