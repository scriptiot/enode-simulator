#ifndef __EVM_MODULE_PWM_H
#define __EVM_MODULE_PWM_H
#include "evm_module.h"

/************************ PWM ****************************/
typedef struct evm_pwm{
    /**
     * @brief  int open(int id)
     * @note   使能pwm
     * @param  id: PWM通道
     * @retval =0：成功 
     *         <0：失败
     */
    int (*open)(int);
    
    /**
     * @brief  int setup(int id, int freq, int duty)
     * @note   配置PWM输出
     * @param  id: 通道
     * @param  freq: 频率
     * @param  duty: 占空比
     * @retval =0：成功 
     *         <0：失败
     */
    int (*setup)(int, int, int);
    
    /**
     * @brief  int close(int id);
     * @note   关闭PWM
     * @param  id: 通道
     * @retval 
     */
    int (*close)(int);
}evm_pwm_t;


evm_pwm_t *evm_module_pwm_getInstance(void);

int evm_module_pwm_init(void);
void evm_module_pwm_destory(void);

evm_err_t evm_module_pwm(evm_t *e);
#endif

