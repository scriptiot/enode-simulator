#ifndef __EVM_MODULE_H
#define __EVM_MODULE_H
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "app_start.h"
#include "evm_module_cffi.h"

typedef struct msg_buffer{
    void *data;
    size_t len;
}msg_buffer_t;

#include "evm_module_eventbus.h"
#include "evm_module_adc.h"
#include "evm_module_device.h"
#include "evm_module_fs.h"
#include "evm_module_http.h"
#include "evm_module_iic.h"
#include "evm_module_gpio.h"
#include "evm_module_pwm.h"
#include "evm_module_rtc.h"
#include "evm_module_sms.h"
#include "evm_module_spi.h"
#include "evm_module_builtin.h"
#include "evm_module_tcp.h"
#include "evm_module_timer.h"
#include "evm_module_uart.h"
#include "evm_module_wdg.h"
#include "evm_module_os.h"

#endif
