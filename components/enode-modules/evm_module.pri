HEADERS += \
    $$PWD/includes/evm_module.h \
    $$PWD/includes/evm_module_adc.h \
    $$PWD/includes/evm_module_builtin.h \
    $$PWD/includes/evm_module_device.h \
    $$PWD/includes/evm_module_eventbus.h \
    $$PWD/includes/evm_module_fs.h \
    $$PWD/includes/evm_module_gpio.h \
    $$PWD/includes/evm_module_http.h \
    $$PWD/includes/evm_module_iic.h \
    $$PWD/includes/evm_module_os.h \
    $$PWD/includes/evm_module_pwm.h \
    $$PWD/includes/evm_module_rtc.h \
    $$PWD/includes/evm_module_sms.h \
    $$PWD/includes/evm_module_spi.h \
    $$PWD/includes/evm_module_tcp.h \
    $$PWD/includes/evm_module_timer.h \
    $$PWD/includes/evm_module_uart.h \
    $$PWD/includes/evm_module_wdg.h


SOURCES += \
    $$PWD/src/adaptor_pc/evm_adaptor_gpio.c \
    $$PWD/src/module/evm_module_builtin.c \
    $$PWD/src/module/evm_module_dlfcn.c \
    $$PWD/src/module/evm_module_eventbus.c \
    $$PWD/src/module/evm_module_gpio.c \
    $$PWD/src/module/evm_module_device.c \
    $$PWD/src/module/evm_module_timer.c \
    $$PWD/src/module/evm_module_uart.c \
    $$PWD/src/module/evm_module_http.c \
    $$PWD/src/module/evm_module_wdg.c \
    $$PWD/src/module/evm_module_tcp.c \
    $$PWD/src/module/evm_module_rtc.c \
    $$PWD/src/module/evm_module_sms.c \
    $$PWD/src/module/evm_module_spi.c \
    $$PWD/src/module/evm_module_iic.c \
    $$PWD/src/module/evm_module_pwm.c \
    $$PWD/src/module/evm_module_adc.c \
    $$PWD/src/module/evm_module_fs.c \
    $$PWD/src/module/evm_module_os.c

SOURCES += \
    $$PWD/src/adaptor_pc/evm_adaptor_eventbus.c \
    $$PWD/src/adaptor_pc/evm_adaptor_adc.c \
    $$PWD/src/adaptor_pc/evm_adaptor_device.c \
    $$PWD/src/adaptor_pc/evm_adaptor_rtc.c \
    $$PWD/src/adaptor_pc/evm_adaptor_sms.c \
    $$PWD/src/adaptor_pc/evm_adaptor_spi.c \
    $$PWD/src/adaptor_pc/evm_adaptor_iic.c \
    $$PWD/src/adaptor_pc/evm_adaptor_wdg.c \
    $$PWD/src/adaptor_pc/evm_adaptor_tcp.c \
    $$PWD/src/adaptor_pc/evm_adaptor_timer.c \
    $$PWD/src/adaptor_pc/evm_adaptor_fs.c \
    $$PWD/src/adaptor_pc/evm_adaptor_http.c \
    $$PWD/src/adaptor_pc/evm_adaptor_uart.c \
    $$PWD/src/adaptor_pc/evm_adaptor_pwm.c \
    $$PWD/src/adaptor_pc/evm_adaptor_os.c


INCLUDEPATH += $$PWD/includes


HEADERS += \
    $$PWD/../message_queue/message_queue.h
SOURCES += \
    $$PWD/../message_queue/message_queue.c
INCLUDEPATH += $$PWD/../message_queue


HEADERS += \
    $$PWD/../evm-list/evmlist.h
SOURCES += \
    $$PWD/../evm-list/evmlist.c
INCLUDEPATH += $$PWD/../evm-list
