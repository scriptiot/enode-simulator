#ifndef __EVUE_H
#define __EVUE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include "evm_app.h"

/**
 * @brief evue
 * @note  虚拟机初始化函数
 * @param heap          虚拟机堆分配大小
 * @param stack         虚拟机栈分配大小
 * @param srciptpath    脚本文件路径
 * @return  0：成功
 *         -1：失败
 */
int app_start(evm_t * e, const char* srciptpath);


#ifdef __cplusplus
}
#endif

#endif
