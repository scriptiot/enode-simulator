# ENode Simulator Qt Project

本项目是ENode Qt creator的工程项目，可以克隆本从库到本地，扩展模块，构建自己的模拟器。

## 快速体验
```
git clone https://gitee.com/scriptiot/enode-simulator.git
```

使用Qt creator（该工程使用版本：v5.15.2）打开enode.pro文件。

配置好编译器后，在Qt creator 项目中，按下图所示，更换为你自己电脑上的路径：

![Qt creator项目配置](./assets/20211124141727.jpg)

```
Command line arguments: 表示要传递给enode.exe的启动参数，该参数表示要运行的脚本路径

Working directory: 表示当先enode.exe所处路径
```

配置完成后，点击运行。

## 测试用例

所有测试用例位于test目录下，默认启动文件为app.js，可以修改`Command line arguments`来运行不同的测试用例。

## 开发文档

[ENode小程序应用开发标准1.0](https://www.yuque.com/books/share/136a6b29-2676-441f-a025-ef23e6d3fd11)

## 相关链接

[EVM Github](https://github.com/scriptiot/evm)

[EVM Gitee](https://gitee.com/scriptiot/evm)

[EVUE小程序开发文档 V1.0](https://www.yuque.com/books/share/00225bdd-c40a-4067-b3e6-662d02d9a2b5)

[字节码科技官网](https://evmiot.com/)
