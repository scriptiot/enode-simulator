function ThrowError(filename) {
    this.filename = filename;

    this.trace = function (funcname, line, type, message) {
        var error1_trace = "Js Traceback(most recent call last): \n";
        var error_file =
            "\tFile <" + this.filename + "> " + "line " + line + " <module>\n";
        var error_function = "\tFunction <" + funcname + "> '\n ";
        var error_message = "\t" + type + ":" + message + "\n";
        var message =
            error1_trace + error_file + error_function + error_message;
        console.log(message);
        return message;
    };
}

var Err = new ThrowError("require.js");

function _require(module) {
    var pathList = [];

    if (globalThis.platform == "nodejs") {
        var extname = module.split(".");
        extname = extname[extname.length - 1];
        if (extname != "js") {
            module = module + ".js";
        }
        pathList = ["./"];
    } else if (globalThis.platform == "evm") {
        pathList = [""];
    }

    if (
        globalThis.package != undefined &&
        globalThis.package.path != undefined
    ) {
        var package = JSON.parse(JSON.stringify(globalThis.package));
        for (var i = 0; i < package.path.length; i++) {
            if (package.path[i] != "") {
                pathList.push(package.path[i]);
            }
        }

        var mod = "";
        var result = undefined;
        for (var i = 0; i < pathList.length; i++) {
            if (pathList[i][pathList[i].length - 1] == "/") {
                mod = pathList[i] + module;
            } else {
                if (pathList[i] != "") {
                    mod = pathList[i] + "/" + module;
                } else {
                    mod = module;
                }
            }

            try {
                result = require(mod);
                if (result != undefined) {
                    return result;
                }
            } catch (e) {
                // console.log(e)
                if (globalThis.platform == "nodejs") {
                    if (e.code != 'MODULE_NOT_FOUND') {
                        console.log(e.name)
                        console.log(e.message)
                    }
                }
                continue;
            }
        }

        if (result == undefined) {
            throw Err.trace(
                "_require",
                66,
                "module",
                "module: " + mod + " not found"
            );
        }
    }
}

globalThis._require = _require;
