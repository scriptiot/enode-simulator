#-*- coding: UTF-8 -*- 
#!/usr/bin/python

import sys
import json
import types
import traceback
import inspect

def argc(value):
    ret = inspect.getfullargspec(value)
    return len(ret.args)

def typeof(value):
    _type = 'None'
    if isinstance(value,bool):
        _type = "bool"
    elif isinstance(value,int):
        _type = "int"
    elif isinstance(value,str):
        _type = "str"
    elif isinstance(value,float):
        _type = "float"
    elif isinstance(value,list):
        _type = "list"
    elif isinstance(value,tuple):
        _type = "tuple"
    elif isinstance(value, dict):
        _type = "dict"
    elif isinstance(value,set):
        _type = "set"
    elif isinstance(value, types.FunctionType):
        _type = 'function'
    elif isinstance(value, types.MethodType):
        _type = 'method'
    else:
        if value is not None:
            _t = str(type(value))
            _type = _t.split('\'')[1]
            if _type == 'type':
                _type = 'class'
    return _type

def metainfo(obj):
    try:
        ret = []
        keys = [e for e in dir(obj) if not e.startswith('_')]
        for key in keys:
            info = {}
            info['key'] = key
            value = getattr(obj, key)
            if isinstance(value,tuple):
                value = list(value)
            info['type'] = typeof(value)
            info['id'] = id(value)
            if info['type'] is not None:
                ret.append(info)
        return json.dumps(ret, indent=4)
    except:
        print(traceback.format_exc())
        return "{}"

def pyId(obj):
    return id(obj)

def pyDict(jsonstr):
    ret = json.loads(jsonstr)
    return ret

def addPath(path):
    sys.path.append(path)
