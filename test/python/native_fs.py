import os

print("fs loaded......")

def size(path):
    size = os.path.getsize(path)
    return size;

def exists(path):
    return os.path.exists(path)

def rename(oldpath, newpath):
    return os.rename(oldpath, newpath)

def remove(oldpath):
    return os.removedirs(oldpath)

def readFile(path, mode):
    buffer = None
    with open(path, mode) as f:
        buffer = f.read()
    return buffer

def writeFile(path, buf, mode):
    result = -1
    with open(path, mode) as f:
        result = f.write(buf)
    return result