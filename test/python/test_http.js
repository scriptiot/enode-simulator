var http = require("http.js");

function test() {
    http.request({
        url: "https://way.jd.com/he/freeweather?city=beijing&appkey=9a1a8a51ff8c59047a556f33f8adee90",
        method: "GET",
        data: null,
        responseType: "json",
        timeout: 5000,
        headers: ["User-Agent: PostmanRuntime/7.28.4", "Accept: application/json"],
        callback: function(res) {
            console.log("请求成功！！！")
            console.log(res.statusCode)
            console.log(res.headers)
            console.log(res.data)
        },
        error: function(err) {
            console.log("请求失败~~~")
            console.log(err.statusCode)
            console.log(err)
        },
    })
    http.request({
        url: "http://dev.wanliwuhan.com/uploads/pingfanzhilu.jpg",
        method: "GET",
        data: null,
        fileName: "pingfanzhilu.jpg",
        responseType: "file",
        timeout: 5000,
        headers: ["User-Agent: PostmanRuntime/7.28.4", "Accept: application/json"],
        callback: function(res) {
            console.log("请求成功！！！")
            console.log(res.statusCode)
            console.log(res.headers)
            console.log(res.data)
        },
        error: function(err) {
            console.log("请求失败~~~")
            console.log(err.statusCode)
            console.log(err)
        },
    })
}