var error = require("./error.js");
var Err = new error.ThrowError("require.js");

var config = require("./config.json");
var pjs = require("./python.js");
pjs.init_python(config.pythonDir, config.executeDir);

if (globalThis.pjs == undefined) {
    globalThis.pjs = pjs;
}

function _require(module) {
    if (module ==undefined || module.length < 1) throw Err.trace("_require", 7, "MODULE_NAME_UNDEFINED", "module can not be null");

    var new_name = module;
    // 如果是相对路径，这里就存在问题了
    // if (new_name[0] != "." && new_name[1] != "/") {
    //     new_name = "./" + new_name;
    // }

    var name_arr = new_name.split(".");
    console.log(name_arr);
    console.log(typeof name_arr)
    console.log(name_arr.length)
    if (name_arr.length >= 2 && name_arr[name_arr.length - 1] != "js") {
        new_name = new_name + ".js"
    }
    
    console.log("+++++++1990+++++++");
    console.log(new_name);
    return require(new_name);
}

globalThis._require = _require;