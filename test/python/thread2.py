# -*- coding: utf-8 -*-
import time
import threading
from datetime import datetime
import logging
from logging import handlers

class Logger(object):
    level_relations = { # 日志级别关系映射
        'debug':logging.DEBUG,
        'info':logging.INFO,
        'warning':logging.WARNING,
        'error':logging.ERROR,
        'crit':logging.CRITICAL
    }

    def __init__(self, filename, level='info', when='D', backCount=3, fmt='%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s'):
        self.logger = logging.getLogger(filename)
        format_str = logging.Formatter(fmt) #设置日志格式
        self.logger.setLevel(self.level_relations.get(level)) # 设置日志级别
        sh = logging.StreamHandler() # 往屏幕上输出
        sh.setFormatter(format_str) # 设置屏幕上显示的格式
        th = handlers.TimedRotatingFileHandler(filename=filename, when=when, backupCount=backCount, encoding='utf-8') # 往文件里写入#指定间隔时间自动生成文件的处理器
        # 实例化TimedRotatingFileHandler
        # interval是时间间隔，backupCount是备份文件的个数，如果超过这个个数，就会自动删除，when是间隔的时间单位，单位有以下几种：
        # S 秒
        # M 分
        # H 小时、
        # D 天、
        # W 每星期（interval==0时代表星期一）
        # midnight 每天凌晨
        th.setFormatter(format_str) # 设置文件里写入的格式
        self.logger.addHandler(sh) # 把对象加到logger里
        self.logger.addHandler(th)

_Log = Logger('./running.log', level='info')
logger = _Log.logger

def callback():
    log = Logger('./running.log', level='info')
    log.logger.debug('debug')
    log.logger.info('info')
    log.logger.warning('警告')
    log.logger.error('报错')
    log.logger.critical('严重')

    count = 0

    while count < 100:
        count += 1
        log.logger.info("callback execute, current count is {} .... {}".format(count, datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        print("callback execute.... {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        time.sleep(1)

def test_func0():
    logger.info("test_func0")
    return None

def test_func1(arg1):
    logger.info("test_func1({})".format(arg1))
    return arg1

def test_func2(arg1, arg2):
    logger.info("test_func2({}, {})".format(arg1, arg2))
    return arg1, arg2

def test_func3(arg1, arg2, arg3):
    logger.info("test_func3({}, {}, {})".format(arg1, arg2, arg3))
    return arg1, arg2, arg3

def test_func4(arg1, arg2, arg3, arg4):
    logger.info("test_func3({}, {}, {}, {})".format(arg1, arg2, arg3, arg4))
    return {
        'arg1': arg1,
        'arg2': arg2,
        'arg3': arg3,
        'arg4': arg4
    }

def test_func5(arg1, arg2, arg3, arg4, arg5):
    logger.info("test_func5({}, {}, {}, {}, {})".format(arg1, arg2, arg3, arg4, arg5))
    return arg1, arg2, arg3, arg4, arg5

def test_func6(arg1, arg2, arg3, arg4, arg5, arg6):
    logger.info("test_func6({}, {}, {}, {}, {}, {})".format(arg1, arg2, arg3, arg4, arg5, arg6))
    return arg1, arg2, arg3, arg4, arg5, arg6

def test_func7(arg1, arg2, arg3, arg4, arg5, arg6, arg7):
    logger.info("test_func7({}, {}, {}, {}, {}, {}, {})".format(arg1, arg2, arg3, arg4, arg5, arg6, arg7))
    return arg1, arg2, arg3, arg4, arg5, arg6, arg7

def test_func8(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8):
    logger.info("test_func8({}, {}, {}, {}, {}, {}, {})".format(arg1, arg2, arg3, arg4, arg5, arg6, arg7))
    return arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8

def test_func9(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9):
    logger.info("test_func9({}, {}, {}, {}, {}, {}, {}, {}, {})".format(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9))
    return "arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9"

def hello_world(a):
    print(".............helloworld................")
    print(type(a))
    print(a.keys())
    print(a.values())
    return {'a':1, 'b': '123223', 'c': 3.03, 'd': {'a':2, 'b': 'advdads', 'c': 4.03}, 'e': [blist, blist, blist]}

def start_thread():
    t = threading.Thread(target=callback, args=[])
    t.setDaemon(True)
    t.start()
    # t.join()

def native_print(obj):
    print(obj)

def main():
    # import evm
    # print(evm)
    # print(dir(evm))
    pass


aInt = 1
bFloat = 2.001
bstring = '112321121221xzzxaAASAS'
bunicode = u'中国'
blist = [1, 'xxxxx', 3.096]
bdict = {'a':1, 'b': '123223', 'c': 3.03, 'd': {'a':2, 'b': 'advdads', 'c': 4.03}, 'e': [blist, blist, blist]}
cSet = ()
ddict = {}
