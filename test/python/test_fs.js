var require = globalThis._require;
var fs = require("fs.js")

function test() {
    console.log("************** FS MODULE **************");
    var a = fs.exists("1111.txt");
    console.log("exists:" + a);

    if (a) {
        var b = fs.readFile("1111.txt", "rb+");
        console.log("readFile:" + b);
    }

    fs.writeFile("1111.txt", "this is fs writeFile ", "ab+");

    var size = fs.size("1111.txt");
    console.log("size:" + size);

    var buffer = fs.readFile("1111.txt", "rb");
    console.log("readFile:" + buffer);

    var obj = {
        name: "evue_launcher",
        version: "1.0",
    };

    var ret = fs.writeFile("xxxxx.txt", JSON.stringify(obj), "ab+");
    if (ret == true) {
        console.log("writeFile success");
    } else {
        console.log("writeFile failed");
    }

    console.log("*******************************************");
}

test()