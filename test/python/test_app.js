require("require.js")

var modules = require("@native.modules");
var error = require("error.js")


function test_func(thread) {
    setInterval(function() {
        ret = thread.test_func4(1, 2.02, 'string', { 'a': 123 });
        console.log(ret, 2);
    }, 40)
    console.log(thread, 4)
    ret = thread.test_func0();
    console.log(ret, 2);

    ret = thread.test_func1(1);
    console.log(ret, 2);

    ret = thread.test_func2(1, 2.02);
    console.log(ret, 2);

    ret = thread.test_func3(1, 2.02, 'string');
    console.log(ret, 2);

    ret = thread.test_func4(1, 2.02, 'string', { 'a': 123 });
    console.log(ret, 2);

    ret = thread.test_func5(1, 2.02, 'string', { 'a': 123 }, [1, 2, 3]);
    console.log(ret, 2);

    ret = thread.test_func6(1, 2.02, 'string', { 'a': 123 }, [1, 2, 3], { 'a': { 'a1': { 'a2': 10 } } });
    console.log(ret, 4);

    ret = thread.test_func7(1, 2.02, 'string', { 'a': 123 }, [1, 2, 3], { 'a': { 'a1': { 'a2': 10 } } }, 7);
    console.log(ret, 4);

    ret = thread.test_func8(1, 2.02, 'string', { 'a': 123 }, [1, 2, 3], { 'a': { 'a1': { 'a2': 10 } } }, 7, 8);
    console.log(ret, 4);

    ret = thread.test_func9(1, 2.02, 'string', { 'a': 123 }, [1, 2, 3], { 'a': { 'a1': { 'a2': 10 } } }, 7, 8, 9);
    console.log(ret, 4);

}

function test_thread(thread) {
    thread.start_thread()
}

function throwError(module, filename, funcname, line) {

}

function register(modulename, path, funcs) {
    var ffi = require("@system.ffi")
    var dlfcn = require("@system.dlfcn")
    var lib = ffi.NULL;
    if (path != ffi.NULL) {
        lib = dlfcn.dlopen(path, 1);
    }
    var f = [];
    for (var i = 0; i < funcs.length; i++) {
        var item = funcs[i];
        var name = item[0];
        var signature = item[1];
        var funcPointer = dlfcn.dlsym(lib, name);
        f.push([name, funcPointer, signature])
    }
    return ffi.register(modulename, f)
}

function onCreate() {
    var error = new error.ThrowError("app.js", "onCreate");

    modules.require("dlfcn");
    modules.require("ffi");
    modules.require("system");
    modules.require("http");
    modules.require("fs");

    var pjs = require("./python.js");
    // python = pjs.init_python("/usr/lib/x86_64-linux-gnu/libpython3.7m.so", ['../evue-simware-simulator/test/test_python']);
    pjs.init_python("D:\\Program Files\\Python\\python38.dll", ['../../evue-simware-simulator/test/python']);
    console.log(1112222)
    var thread = pjs.py_import("thread2");
    console.log(7778888)
    console.log(thread);
    thread.main();

    // var ffi = require("@system.ffi")
    // var ffitest = require("@system.ffitest")
    // console.log(ffitest.test_dddddd(1.205, 2.503, 1.03, 1.04, 1.05));
    // console.log(ffitest.test_ddddddd(1.205, 2.503, 1.03, 1.04, 1.05, 1.06));
    // console.log(ffitest.test_dddddddd(1.205, 2.503, 1.03, 1.04, 1.05, 1.06, 1.07));
    // console.log(ffitest.test_ddddddddd(1.205, 2.503, 1.03, 1.04, 1.05, 1.06, 1.07, 1.08));
    // console.log(ffitest.test_dddddddddd(1.205, 2.503, 1.03, 1.04, 1.05, 1.06, 1.07, 1.08, 1.09));
    // console.log(ffitest.test_fff(1.205, 2.503));
    // console.log(ffitest.test_fii(1, 2));
    // console.log(ffitest.test_ffffffffff(1.205, 2.503, 1.03, 1.04, 1.05, 1.06, 1.07, 1.08, 1.09));
    // console.log(ffi.s2o(ffitest.st, ffitest.my_struct_descr), 2)

    // var dlfcn = require("@system.dlfcn")
    // p = dlfcn.dlsym(ffi.NULL, "test_fff")
    // console.log(ffi.call(p, "test_fff", "fff", 1.1, 2.0))

    // var isSuccess = register("ffitest", ffi.NULL, [
    //     ["test_fff", "fff"]
    // ])
    // if (isSuccess) {
    //     ffitest = require("ffitest")
    //     console.log(ffitest.test_fff(1.2, 3.5))
    // }

    // error.trace(65, "TestError", "xxxzxzxxzxzzx");
    // test_func(thread);
    test_thread(thread);
}


function onDestory() {
    modules.destory_fs()
    modules.destory_http()
    modules.destory_system()
}