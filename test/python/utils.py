import os
import random
import string

def random_string(length=32):
    return ''.join(random.sample(string.ascii_letters + string.digits, length))

def basename(filename):
    return os.path.basename(filename)