var require = globalThis._require;
var tcp = require("tcp.js");

function test() {
    console.log("************** TCP MODULE **************");
    var tcpClient = tcp.createClient({
        ip: "172.22.48.1",
        port: 1000,
        success: function () {
            console.log("tcp client connect success");
        },
        fail: function () {
            console.log("tcp client connect failed");
        },
    });

    tcpClient.on("data", function (data) {
        console.log("tcp receive data: ");
        console.log(data);
        // tcpClient.close();
    });

    tcpClient.on("close", function () {
        console.log("tcp client closed");
    });

    tcpClient.on("error", function (err) {
        console.log("tcp client error: " + err);
    });

    setInterval(function() {
        tcpClient.send({
            data: "hello, this is tcp client test",
        });
    }, 2000);
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}

test()
