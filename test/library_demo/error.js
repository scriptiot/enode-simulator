function CFFIError() {
    var info = globalThis.cffi_error

    if (info == undefined) {
        return;
    }

    var ctypes = {
        "b": "bool",
        "i": "char|short|int|long",
        "d": "double",
        "s": "0-terminated char *",
        "v": "undefined",
        "p": "c pointer",
        "j": "evm_val_t"
    }

    var error1_trace = "C FFI Traceback(most recent call last): \n";
    var error_file = "\tFile <" + info.filename + "> " + "line " + info.line + " <module>\n";
    var error_function = "\tFunction <" + info.funcname + "> signature <'" + info.signature + "'> \n ";
    var error_message = "CFFIERROR";
    var s = info.signature[info.error_index]
    if (info.error_code <= 6) {
        var errortype = typeof info.type;
        error_message = "ARGError: arg type [" + s + "] is [" + errortype + "] should be[" + ctypes[s] + "]";
    } else if (info.error_code == 7) {
        error_message = "ARGError: arg type [" + s + "] not support";
    } else if (info.error_code == 8) {
        error_message = "ReturnError: c pointer type is NULL, should be [ c pointer]";
    } else if (info.error_code == 9) {
        error_message = "ReturnError: evm type is NULL should be [ evm_val_t ]";
    } else if (info.error_code == 10) {
        error_message = "ReturnError: string created failed, no enough memory";
    } else if (info.error_code == 11) {
        error_message = "ReturnError: return type not support";
    }

    error_message = error_message + "\n";

    return error1_trace + error_file + error_function + error_message
}