var gpio = require("gpio.js")
var led = gpio.open({
    id: 'led'
});

led.writeValue(1);
led.readValue();
led.writeValue(0);
led.readValue();
led.toggle();