

function writeValue(value) {
    ngpio.writeValue(value)
}
function readValue() {
    return ngpio.readValue()
}
function toggle() {
    ngpio.toggle()
}
function onIRQ(obj) {
    ngpio.onIRQ(obj.trigger)
    obj.cb()
}
function close() {
    ngpio.close()
}