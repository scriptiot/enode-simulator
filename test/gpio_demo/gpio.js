var fs = require("fs.js")
var os = require("os.js")

var ngpio = undefined
if (os.platform() == "windows" || os.platform() == "linux") {
    ngpio = require("ngpio.js")
} else {
    ngpio = require("@native.gpio")
}

config = fs.readFile("config.json", "r")
config = JSON.parse(config)

function Gpio(options) {
    this.type = options.type
    this.port = options.port
    this.dir = options.dir
    this.pull = options.pull
    this.intMode = options.intMode

    this.writeValue = function (value) {
        ngpio.writeValue(value)
    }
    this.readValue = function () {
        return ngpio.readValue()
    }
    this.toggle = function () {
        ngpio.toggle()
    }
    this.onIRQ = function (obj) {
        ngpio.onIRQ(obj.trigger)
        obj.cb()
    }
    this.close = function() {
        ngpio.close()
    }
}

function open(opt) {
    var id = opt.id
    var io = config["io"]
    // 判断
    if (!id) {
        //
    }
    if (!io) {
        //
    }
    conf = io[id]
    console.log(conf)
    return new Gpio(conf)
} 
