if (globalThis.global != undefined) {
    globalThis.platform = "nodejs";
} else {
    globalThis.platform = "evm";
}

var config = require("./package.json");
require("./require.js");
var require = globalThis._require;

function onCreate() {
    globalThis.package = config;

    require("eventbus.js");
    if (globalThis.package.main != undefined) {
        require(globalThis.package.main);
    }
}

if (globalThis.platform == "nodejs") {
    // nodejs env
    onCreate();
}