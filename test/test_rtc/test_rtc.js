var require = globalThis._require;
var rtc = require("rtc.js");

function test() {
    console.log("************** RTC MODULE **************");

    var rtc1 = rtc.open();
    console.log(rtc1);

    var date = rtc1.getTime();
    console.log(date);

    var res = rtc1.setTime({
        year: 2021,
        month: 11,
        date: 2,
        hours: 14,
        minutes: 52,
        seconds: 0,
    });
    console.log(res);
    rtc1.close();
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}

test()