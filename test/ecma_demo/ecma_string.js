// 如果支持prototype，就可以重写类原型方法
function endsWith(searchvalue) {

}

function includes(searchvalue) {

}

function lastIndexOf(searchvalue) {

}

function startsWith(searchvalue) {

}

function substr(start,length) {

}

function substring(from, to) {

}

function toLowerCase() {

}

function toUpperCase() {

}

function trim() {

}

if (globalThis.global != undefined) {
    module.exports = {
        endsWith,
        includes,
        lastIndexOf,
        startsWith,
        substr,
        substring,
        toLowerCase,
        toUpperCase,
        trim
    }
}