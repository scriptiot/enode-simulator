const http = require("http");

const req = http.request(
    {
        hostname: "http://httpbin.org/get",
        method: "GET",
    },
    function (res) {
        console.log(`STATUS: ${res.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
        res.setEncoding("utf8");
        res.on("data", function (chunk) {
            console.log(`BODY: ${chunk}`);
        });
        res.on("end", function () {
            console.log("No more data in response.");
        });
    }
);

req.on("error", function (e) {
    console.error(`problem with request: ${e.message}`);
});

// 将数据写入请求正文
// req.write();
req.end();

console.log(req);