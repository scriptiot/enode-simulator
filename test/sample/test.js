// const sleep = (time) => {
//     return new Promise(resolve => setTimeout(resolve, time));
// }

// console.log(1);

// sleep(2000).then(() => {
//     console.log(2);
// });

// console.log(3);

// 上述代码打印顺序：1,3,2，无法实现阻塞需求

const sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay))

const repeatedGreetings = async () => {
    await sleep(1000)
    console.log(1)
    await sleep(1000)
    console.log(2)
    await sleep(1000)
    console.log(3)
}

repeatedGreetings()

// 以上代码等同于以下代码

setTimeout(() => console.log(1), 1000)
setTimeout(() => console.log(2), 2000)
setTimeout(() => console.log(3), 3000)