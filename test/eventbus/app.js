require("../../libjs/eventbus.js");

function test(opt){
    var id = opt["id"];
    var data = opt["data"];
    console.log("test callback");
    console.log(id);
    console.log(data);
}

function onCreate() {
    globalThis.eventbus.on("eventtest", 1, "test", test);
    globalThis.eventbus.emit("eventtest", 1, "test", 1, [{"id":1,"data":"helloworld"}]);
}