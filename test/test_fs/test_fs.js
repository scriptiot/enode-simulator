var require = globalThis._require;
var fs = require("fs.js");

function test() {
    console.log("************** FS MODULE **************");
    var a = fs.exists("1111.txt");
    console.log("exists:" + a);

    if (a) {
        var b = fs.readFile("1111.txt", "r+");
        console.log("readFile:" + b);
    }

    var res = fs.writeFile("1111.txt", "this is fs writeFile, Hello,World", "w+");
    console.log("writeFile size:" + res);

    var size = fs.size("1111.txt");
    console.log("size:" + size);

    var current = fs.getcwd();
    console.log("++++++++++++++++++++++++++")
    console.log(current);
    var content = fs.readFile("app.js", "r");
    console.log(content);

    var buffer = fs.readFile("1111.txt", "r");
    console.log("readFile:" + buffer);

    var res = fs.writeFile("2222.txt", [1,2,3,4,5,6], "wb+");
    console.log("writeFile size:" + res);

    buffer = fs.readFile("2222.txt", "rb");
    console.log("readFile:");
    console.log(buffer)

    var obj = {
        "name": "evue_launcher",
        "version": "1.0"
    }

    var ret = fs.writeFile("xxxxx.txt", JSON.stringify(obj), "w+");
    if (ret >= 0) {
        console.log("writeFile success");
    } else {
        console.log("writeFile failed");
    }

    console.log("*******************************************");
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}

test()
