var require = globalThis._require;
var gpio = require("gpio.js");

function test() {
    console.log("************** GPIO MODULE **************");
    // led
    var led = gpio.open({
        index: 1,
        dir: gpio.OUT,
    });
    // set led gpio level
    led.write(1);
    led.write(0);
    // led toggle
    led.toggle();

    // key
    var key = gpio.open({
        index: 2,
        dir: gpio.IN,
    });
    console.log("*********************1111")
    console.log(key)
    //key trigger callback
    key.on("rising", function () {
        console.log("key pressed\n");
    });
    console.log("*********************2222")
    // release led&key
    led.close();
    key.close();
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}
