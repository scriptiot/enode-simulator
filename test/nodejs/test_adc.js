var require = globalThis._require;
var adc = require("adc.js");

function test() {
    console.log("**************** ADC MODULE **************");
    // voltage
    var vol = adc.open({
        id: 0,
        rate: 100000,
    });

    // read voltage
    var value = vol.read();

    console.log("voltage value is " + value);
    console.log(value);
    vol.close();
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}
