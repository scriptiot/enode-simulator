const net = require("net");

const server = net.createServer((socket) => {
    server.getConnections((err, count) => {
        if (err) {
            console.warn(err);
        } else {
            console.log(`当前有${count}个连接`);
        }
    });

    socket.on("data", (res) => {
        console.log(res);
        socket.write(res);
        // socket.end();
    });

    socket.on("error", () => {
        console.log("error");
    });
    socket.on("end", () => {
        server.close();
    });
});

server.on("error", (err) => {
    throw err;
});
server.listen(12345, () => {
    console.log("server bound");
});
