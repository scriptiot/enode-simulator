var require = globalThis._require;
var uart = require("uart.js");

function test() {
    console.log("************** UART MODULE **************");

    var serial = uart.open({
        id: 8,
        baud: 115200,
    });

    if (serial == undefined) {
        console.log("serial is not exist");
        return undefined;
    }
    var msgbuf = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    var msgbuf1 = "hello world";

    // uart write
    serial.write(msgbuf);
    serial.write(msgbuf1);

    // uart read
    var value = serial.read({
    "len":8,
    "timeout":1000
    });
    console.log("sensor value is ");
    console.log(value);

    // uart data event
    serial.on("recv", function (data) {
        console.log("uart receive data is ");
        console.log(data);
    });

    // uart close
    // serial.close();
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}
