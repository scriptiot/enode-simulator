var require = globalThis._require;
var pwm = require("pwm.js");

function test() {
    console.log("************** PWM MODULE **************");
    // motor start
    var motor = pwm.open(1);

    // get motor option
    var freq = 1000;
    var duty = 50;

    console.log("pwm config freq is " + freq + " duty is " + duty);

    // set motor option
    motor.setup(duty, freq);
    // motor stop
    motor.close();
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}
