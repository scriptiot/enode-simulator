var require = globalThis._require;
var timer = require("timer.js");

function test() {
    console.log("================== test timer ==================");
    function callback(obj) {
        console.log("timer_callback");
        console.log(obj);
    }

    var timer1 = timer.open(1);
    timer1.setTimeout(function (obj) {
        console.log("timer1 timeout");
        console.log(obj);
    }, 1000);

    timer1.setInterval(function (obj) {
        console.log("timer1 interval");
        console.log(obj);
    }, 1000);

    timer1.close();
    console.log("================================================");
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}

test()