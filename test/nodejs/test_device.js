var require = globalThis._require;
var device = require("device.js");

console.log("/////////////////////////");

function test() {
    console.log("************** DEVICE MODULE **************");
    var imei = device.imei();
    console.log("IMEI:" + imei);

}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}
