module.exports = {
    open: function(id, mode, addr, freq) {
        console.log(`[IIC] ===> id: ${id}, mode:${mode}, addr:${addr}, freq:${freq}`);
        return 0;
    },
    close: function(id) {
        console.log(`[IIC] ===> id:${id}`);
    },
    read: function(id, reg, buffer, size) {
        console.log(`[IIC] ===> id: ${id}, reg:${reg}, buffer:${buffer}, size:${size}`)
    },
    write: function(id, reg, buffer, size) {
        console.log(`[IIC] ===> id: ${id}, reg:${reg}, buffer:${buffer}, size:${size}`)
    }
}