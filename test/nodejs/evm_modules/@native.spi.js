module.exports = {
    open: function(id, mode, freq, polarity, phase, firstbit) {
        console.log(`[SPI] ===> open id:${id}, mode:${mode}, freq:${freq}, polarity:${polarity}, phase:${phase}, firstbit:${firstbit}`)
    },
    close: function(id) {
        console.log(`[SPI] ===> close id:${id}`)
    },
    read: function(id, buffer, num) {
        console.log(`[SPI] ===> read id:${id}, buffer:${buffer}, num:${num}`)
    },
    write: function(id, data, size) {
        console.log(`[SPI] ===> write id:${id}, data:${data}, size:${size}`)
    }
}