var require = globalThis._require;
var config = require("./config.json");

/******* 
 * @description: 获取设备IMEI号码
 * @param {String | Buffer} buffer
 * @param {Integer} size
 * @return {String}
 * @author: null
 */
function imei(buffer, size) {
    var length = -1;
    if (Buffer.isBuffer(buffer)) {
        var uuid = Buffer.from(config.imei);
        uuid.copy(buffer);
        length = uuid.length;
    }
    return length;
}

/******* 
 * @description: 延时阻塞多少毫秒，实际上在json中不可用。js所有函数执行都是异步的。但是可以使用迭代器或者await+async+Promise+setTimeout来模拟实现，这需要入侵业务代码。
 * @param {*} ms
 * @return {*}
 * @author: null
 */
function delay(ms) {
    console.log(`[DEVICE] ===> delay, ${ms}`);
}

/******* 
 * @description: 获取平台
 * @param {*} null
 * @return {String}
 * @author: null
 */
function platform() {
    return globalThis.platform;
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        imei,
        delay,
        platform,
    };
}
