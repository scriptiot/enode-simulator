module.exports = {
    setup: function(id, freq, duty) {
        console.log(`[PWM] ===> setup id:${id}, freq:${freq}, duty:${duty}`)
    },
    open: function(id) {
        console.log(`[PWM] ===> open id:${id}`);
    },
    close: function(id) {
        console.log(`[PWM] ===> close id:${id}`);
    },
}