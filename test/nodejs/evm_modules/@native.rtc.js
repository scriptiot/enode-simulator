module.exports = {
    getTime: function(date) {
        console.log(`[RTC] ===> getTime data:${date}`);
        return 1;
    },
    setTime: function(year, month, date, hours, minutes, seconds) {
        console.log(`[RTC] ===> setTime year:${year}, month:${month}, date:${date}, hours:${hours}, minutes:${minutes}, seconds:${seconds}`);
        var res = new Date();
        if (typeof year == "number") res.setFullYear(year);
        if (typeof month == "number") res.setMonth(month);
        if (typeof date == "number") res.setDate(date);
        if (typeof hours == "number") res.setHours(hours);
        if (typeof minutes == "number") res.setMinutes(minutes);
        if (typeof seconds == "number") res.setSeconds(seconds);
        return res;
    },
    createDate: function() {
        console.log(`[RTC] ===> createDate`);
        return new Date();
    },
    destoryDate: function(date) {
        console.log(`[RTC] ===> destoryDate date:${date}`);
    },
    close: function() {
        console.log(`[RTC] ===> close`);
    },
    getFullYear: function(date) {
        console.log(`[RTC] ===> getFullYear date:${date}`);
        return date.getFullYear();
    },
    getMonth: function(date) {
        console.log(`[RTC] ===> getMonth date:${date}`);
        return date.getMonth();
    },
    getDate: function(date) {
        console.log(`[RTC] ===> getDate date:${date}`);
        return date.getDate();
    },
    getHours: function(date) {
        console.log(`[RTC] ===> getHours date:${date}`);
        return date.getHours();
    },
    getMinutes: function(date) {
        console.log(`[RTC] ===> getMinutes date:${date}`);
        return date.getMinutes();
    },
    getSeconds: function(date) {
        console.log(`[RTC] ===> getSeconds date:${date}`);
        return date.getSeconds();
    }
}