module.exports = {
    setup: function() {},
    read: function(id) {
        console.log(`[PIN] ===> read id:${id}`);
    },
    write: function(id, level) {
        console.log(`[PIN] ===> write id:${id}, level:${level}`);
    },
    toggle: function(id) {
        console.log(`[PIN] ===> toggle id:${id}`);
    },
    on: function(id, __id__, event) {
        console.log(`[PIN] ===> on id:${id}, event:${event}`);
    },
    close: function(id) {
        console.log(`[PIN] ===> close id:${id}`)
    }
}