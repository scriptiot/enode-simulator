function randomString(len) {
    len = len || 32;
    var $seed = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyzogqUuIVvOLl0123456789";
    var maxPos = $seed.length;
    var result = "";
    for (i = 0; i < len; i++) {
        result += $seed.charAt(Math.floor(Math.random() * maxPos));
    }
    return result;
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        id: function(_this) {
            return randomString();
        }
    };
}
