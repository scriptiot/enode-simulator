function read(id) {
    console.log(`[ADC] ===> ${id}`);
    return Math.ceil(Math.random() * 10);
}

function open(id, rate) {
    console.log(`[ADC] ===> ${id},${rate} open`);
    return 0;
}

function close(id) {
    console.log(`[ADC] ===> ${id} closed`);
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        read,
        open,
        close,
    };
}
