function randomString(len) {
    len = len || 32;
    var $seed = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyzogqUuIVvOLl0123456789";
    var maxPos = $seed.length;
    var result = "";
    for (i = 0; i < len; i++) {
        result += $seed.charAt(Math.floor(Math.random() * maxPos));
    }
    return result;
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        id: function (_this) {
            return randomString();
        },
        malloc: function (size) {
            return Buffer.alloc(size);
        },
        free: function (buffer) {
            buffer = null;
            return buffer;
        },
        copy: function (data, buffer, size) {
            var length = -1;
            if (!Buffer.isBuffer(buffer)) {
                buffer = Buffer.from(buffer);
            }
            if (Buffer.isBuffer(buffer) && size > 0) {
                length = buffer.copy(data, 0, 0, size);
            }

            if (length > 0) return true;
            return false;
        },
        toJsArray: function(buffer, size) {
            var res = Buffer.from(buffer)
            return res;
        },
        toJsString: function (buffer, size) {
            if (!buffer) return buffer;
            if (Buffer.isBuffer(buffer)) buffer = buffer.toString("utf8");
            return buffer;
        },
        randomString: randomString
    };
}
