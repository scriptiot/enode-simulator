module.exports = {
    open: function(id, baud, databits, stopbits, parity, buffersize) {
        console.log(`[UART] ===> open id:${id}, baud: ${baud}, databits:${databits}, stopbits:${stopbits}, parity:${parity}, buffersize:${buffersize}`)
    },
    close: function(id) {
        console.log(`[UART] ===> close id:${id}`)
    },
    read: function(id, buffer, len, timeout) {
        console.log(`[UART] ===> read id:${id}, buffer:${buffer}, len:${len}, timeout:${timeout}`)
    },
    write: function(id, data, size) {
        console.log(`[UART] ===> write id:${id}, data:${data}, size:${size}`)
    },
    on: function(id, __id__, event) {
        console.log(`[UART] ===> close id:${id},__id__:${__id__}, event:${event}`)
    }
}