var require = globalThis._require;
var wdg = require("wdg.js");

function test() {
    console.log("************** WDG MODULE **************");

    var wdg1 = wdg.open(3000);
    wdg1.feed();
    wdg1.close();
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}
