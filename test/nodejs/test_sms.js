var require = globalThis._require;
var sms = require("@system.sms");

function test() {
    console.log("************** RTC MODULE **************");
    sms.send("123456789", "helloworld");
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}
