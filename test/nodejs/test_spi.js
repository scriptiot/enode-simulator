var require = globalThis._require;
var spi = require("spi.js");

function test() {
    console.log("************** SPI MODULE **************");

    var msgbuf = [0x10, 0xee];

    var sensor = spi.open({
        id: 1,
        mode: spi.MODE_MASTER,
        freq: 100000,
        firstbit: spi.FIRSTBITS_LSB,
    });

    sensor.write(msgbuf);
    var value = sensor.read(2);

    console.log("sensor value is ");
    console.log(value);

    sensor.close();
}

if (globalThis.platform == "nodejs") {
    module.exports = {
        test: test,
    };
}
