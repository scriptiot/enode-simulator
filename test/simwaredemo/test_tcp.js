var tcp = require("tcp.js");

function test() {
    console.log("************** TCP MODULE **************");
    var tcpClient = tcp.createClient({
        ip: "192.168.31.6",
        port: 8080,
        success: function () {
            console.log("tcp client connect success");
        },
        fail: function () {
            console.log("tcp client connect failed");
        },
    });

    tcpClient.on("data", function (data) {
        console.log("tcp receive data: ");
        console.log(data);
        // tcpClient.close();
    });

    tcpClient.on("close", function () {
        console.log("tcp client closed");
    });

    tcpClient.on("error", function (err) {
        console.log("tcp client error: " + err);
    });
      
    tcpClient.on('error', function(opt) {
        console.log('tcp client error: ' + opt);
    });
      
    tcpClient.send({
        "data":'hello, this is tcp client test'
    });
}
