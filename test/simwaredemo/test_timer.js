var timer = require("timer.js");

function test() {
    console.log("================== test timer ==================");
    function callback(obj) {
        console.log("timer_callback");
        console.log(obj);
    }

    var timer1 = timer.open(1);
    timer1.setTimeout(function (obj) {
        console.log("timer1 timeout");
        console.log(obj);
    }, 1000);
    var timer2 = timer.open(2);

    timer2.setInterval(function(obj) {
        console.log('timer2 interval');
        console.log(obj);
    }, 1000);
    
    timer1.close();
    timer2.close();
    console.log("================================================")
}
