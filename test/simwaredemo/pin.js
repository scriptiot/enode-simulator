var npin = require("@native.pin");

//dir
IN = 0;
OUT = 1;

//pull
NOPULL = 0;
PULLUP = 1;
PULLDOWN = 2;

//level
LOW = 0;
HIGH = 1;

function setup(index, dir, pull, level) {
    return npin.setup(index, dir, pull, level);
}

function write(index, level) {
    return npin.write(index, level);
}

function read(index) {
    var level = npin.read(index);
    if (level == 0) return LOW;
    if (level == 1) return HIGH;
    return undefined;
}

