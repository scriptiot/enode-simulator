const net = require("net");

const client = net.createConnection({ port: 12345, host: "127.0.0.1" }, () => {
    // 'connect' 监听器。
    console.log("connected to server!");
    client.write("world!\r\n");
});
client.on("data", (data) => {
    console.log(data.toString());
    client.end();
});

client.on("close", (res) => {
    console.log("close from server");
    console.log(res);
});

client.on("end", (res) => {
    console.log("disconnected from server");
    console.log(res);
});
