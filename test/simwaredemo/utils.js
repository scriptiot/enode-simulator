/*******
 * @description: 参数检查
 * @param {Object} params 需要检查的参数
 * @param {Object} rules 检查规则
 * @return {Boolean}
 * @author: null
 */
function validate(params, rules) {
    if (params == null || params == undefined) {
        throw new Error("params can not be null or undefined");
    }

    if (rules == null || rules == undefined) {
        throw new Error("rules can not be null or undefined");
    }

    var keys = Object.keys(params),
        key,
        value;
    for (var i = 0; i < keys.length; i++) {
        key = keys[i];
        value = params[key];

        if (rules[key] && rules[key] != undefined) {
            if (rules[key].type == undefined)
                throw new Error("parameter must be have type field");

            var type = rules[key].type;

            // 检查是否为空
            if (
                rules[key].allowEmpty != undefined &&
                rules[key].allowEmpty == false
            ) {
                if (type == "object" && Object.keys(value).length == 0) {
                    throw new Error("parameter " + key + " can not be empty");
                } else if (
                    (type == "array" || type == "string") &&
                    value.length == 0
                ) {
                    throw new Error("parameter " + key + " can not be empty");
                }
            }

            // 检查值是否为null
            if (
                rules[key].allowNull != undefined &&
                rules[key].allowNull == false
            ) {
                if (value == null)
                    throw new Error("parameter " + key + " can not be null");
                // 检查类型
                if (typeof value != type)
                    throw new Error(
                        "parameter " + key + " type must be " + type
                    );
            }

            // 检查值是否为undefined
            if (
                rules[key].allowUndefined != undefined &&
                rules[key].allowUndefined == false
            ) {
                if (value == undefined)
                    throw new Error(
                        "parameter " + key + " can not be undefined"
                    );
                if (typeof value != type)
                    throw new Error(
                        "parameter " + key + " type must be " + type
                    );
            }

            // 默认值赋值
            if (
                rules[key].default != undefined &&
                (value == null ||
                    value == undefined ||
                    value == "" ||
                    (typeof value == "array" && value.length == 0) ||
                    (typeof value == "string" && value.length == 0) ||
                    (typeof value == "object" &&
                        Object.keys(value).length == 0))
            ) {
                params[key] = rules[key].default;
            }
        }
    }

    return params;
}

function isObject(obj) {
    var result = false;
    if (globalThis.__proto__ != undefined) {
        result = Object.prototype.toString.call(obj) === "[object Object]";
    } else {
        result = typeof obj == "object";
    }
    return result;
}

if (globalThis.__proto__ != undefined) {
    const logger = require("node-color-log");
    logger.setLevel("debug");
    logger.setDate(() => new Date().toLocaleTimeString());

    // const message = 'any string you will put into console.log';
    // const setting = {
    //     bold: true,
    //     dim: true,
    //     underscore: true,
    //     reverse: true,
    //     italic: true,
    //     strikethrough: true
    // }

    // logger.log(message);
    // logger.color('red').bgColor('blue')
    //     .bold().italic().dim().reverse().underscore().strikethrough()
    //     .log(message);

    // only set font color
    // logger.fontColorLog('red', message, setting);
    // only set background color
    // logger.bgColorLog('yellow', message, setting);
    // set font and background color
    // logger.colorLog({
    //     font: 'black',
    //     bg: 'yellow'
    // }, message, setting);

    // debug level, with prefix "[DEBUG]"
    // logger.debug(message);
    // Error level, with prefix "[ERROR]"
    // logger.error(message);
    // Info level, with prefix "[INFO]"
    // logger.info(message);
    // Warn level, with prefix "[WARN]"
    // logger.warn(message);

    module.exports = {
        isObject: isObject,
        logger: logger,
        validate: validate,
    };
} else {
    var logger = {
        debug: function (msg) {
            console.log(msg);
        },
        info: function (msg) {
            console.log(msg);
        },
        error: function (msg) {
            console.log(msg);
        },
    };
}
