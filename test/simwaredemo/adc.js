var nadc = require("@native.adc");

function Adc(id) {
    this.id = id;

    this.read = function () {
        var sample = nadc.read(this.id);
        return sample;
    };

    this.close = function () {
        nadc.close(this.id);
    };
}

function open(opt) {
    if(opt == undefined)
        return undefined;
    var id = opt["id"];
    if(id == undefined)
        return undefined;
    var rate = opt["rate"];
    if (nadc.open(id, rate) == 0) {
        return new Adc(id);
    }
    return undefined;
}

