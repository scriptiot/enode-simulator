var gpio = require("gpio.js");
os = require("@system.os")


function test(){
    console.log("************** GPIO MODULE **************")
    // led
    var led = gpio.open({
        "index": 122,
        "mode":gpio.OUT
    });

    // set led gpio level
    led.write(1);
    os.delay(100);
    led.write(0);
    os.delay(100);
    led.write(1);
    os.delay(100);
    led.write(0);
    os.delay(100);
    // led toggle
    led.toggle();    
    os.delay(100);
    led.toggle();    
    os.delay(100);

    // key
    var key = gpio.open({
        "index":45,
        "mode":gpio.IN
    });
    //key trigger callback
    key.on("rising", function () {
        console.log("key pressed\n");
    });

    led.close();
    // key.close();
}
