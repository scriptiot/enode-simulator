function _require(module) {
    if (globalThis.__proto__ != undefined) {
        // 判断下后缀
        var path = require('path');
        var extname = path.extname(module);
        // 如果extname等于json或者是js，则直接require
        // 否则，统统在后面加上.js
        if (extname == ".json" || extname == ".js") {
            module = "./" + module;
        } else {
            module = "./" + module + ".js";
        }
        return require(module);
    } else {
        return require(module);
    }
}

globalThis._require = _require;