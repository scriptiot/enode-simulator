require("eventbus.js");

function main() {
    var device = require("test_device.js");
    var timer = require("test_timer.js");
    var http = require("test_http.js");
    var gpio = require("test_gpio.js");
    var fs = require("test_fs.js");
    var adc = require("test_adc.js");
    var pwm = require("test_pwm.js");
    var uart = require("test_uart.js");
    var rtc = require("test_rtc.js");
    var wdg = require("test_wdg.js");
    var sms = require("test_sms.js");
    var spi = require("test_spi.js");
    var iic = require("test_iic.js");
    var tcp = require("test_tcp.js");

    console.log("************** hello simware **************");
    adc.test();
    gpio.test();
    // uart.test();
    fs.test();
    rtc.test();
    wdg.test();
    pwm.test();
    iic.test();
    sms.test();
    spi.test();
    device.test();
    http.test();
    // timer.test();
    // tcp.test();
    console.log("*******************************************");
}

function onCreate(){
    console.log("onCreate");
    main();
}
