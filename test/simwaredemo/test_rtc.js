var rtc = require("rtc.js");

function test(){
    console.log("************** RTC MODULE **************");
    
    var rtc1 = rtc.open();
    console.log(rtc1);

    var date = rtc1.getTime();
    console.log(date);

    rtc1.setTime({
        "year":2021,
        "month":11,
        "date":11,
        "hours":15,
        "minutes":47,
        "seconds":0
    })

    var id = setInterval(function(){
        var date = rtc1.getTime();
        console.log(date);
    },1000);

    setTimeout(function(){
        clearInterval(id);
        rtc1.close();
    },10000);
}
