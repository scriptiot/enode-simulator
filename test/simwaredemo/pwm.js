var npwm = require("@native.pwm");

function PWM(id) {
    this.id = id;

    this.setup = function (freq, duty) {
        if (
            freq == undefined ||
            duty == undefined ||
            typeof freq != "number" ||
            typeof duty != "number"
        )
            return undefined;
        npwm.setup(this.id, freq, duty);
    };

    this.close = function () {
        npwm.close(this.id);
    };
}

function open(id) {
    if (npwm.open(id) == 0) return new PWM(id);
    return undefined;
}
