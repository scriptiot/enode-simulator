var nrtc = require("@native.rtc");

YEAR_BASE = 1970;

function RTC(){
    this.setTime = function(time){

        if(time == undefined)
            return undefined;
        var year = time["year"];
        var month = time["month"];
        var date = time["date"];
        var hours = time["hours"];
        var minutes = time["minutes"];
        var seconds = time["seconds"];

        if (
            year == undefined ||
            month == undefined ||
            date == undefined ||
            hours == undefined ||
            minutes == undefined ||
            seconds == undefined ||
            typeof year != "number" ||
            typeof month != "number" ||
            typeof date != "number" ||
            typeof hours != "number" ||
            typeof minutes != "number" ||
            typeof seconds != "number"
        )
            return undefined;

        if(year < YEAR_BASE)
            year = YEAR_BASE;
        nrtc.setTime(year-YEAR_BASE, month, date, hours, minutes, seconds);
    }

    this.getTime = function(){
        console.log("getTime");
        var date = nrtc.createDate();
        if(date == null)
            return undefined;
        var ret = nrtc.getTime(date);
        if(ret < 0){
            nrtc.destoryDate(date);
            return undefined;
        }
        var time = {};
        time["year"] = nrtc.getFullYear(date) + YEAR_BASE;
        time["month"] = nrtc.getMonth(date);
        time["day"] = nrtc.getDate(date);
        time["hours"] = nrtc.getHours(date);
        time["minutes"] = nrtc.getMinutes(date);
        time["seconds"] = nrtc.getSeconds(date);
        nrtc.destoryDate(date);
        return time;
    }

    this.close = function(){
        nrtc.close();
    }
}

function open()
{
    if(nrtc.open() == 0)
        return new RTC();
    return undefined;
}
