console.log("test.js");

var res = {
    a: 1,
};

function test() {
    console.log("test.js test function");
}

if (globalThis.__proto__ != undefined) {
    const buf = Buffer.alloc(5);

    function bufferChange(buf) {
        buf.fill("a");
    }

    bufferChange(buf);

    console.log(buf);

    var path = require("path"); /*nodejs自带的模块*/
    var extname = path.extname("123"); //获取文件的后缀名

    console.log(extname);

    module.exports = {
        res: res,
        test: test,
    };
}
