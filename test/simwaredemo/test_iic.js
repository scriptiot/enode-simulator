var iic = require("iic.js");

function test(){
    console.log("************** IIC MODULE **************");
    var memaddr = 0x18;
    var msgbuf = [0x10, 0xee];
    // sensor

    var sensor = iic.open({
        "id": 1,
        "mode": iic.MASTER,
        "addr":0x80,
        "freq":400000
    });

    if(sensor == undefined)
        console.log("iic open failed")
    // sensor write
    // sensor.write(msgbuf);
    // var value = sensor.read(2);
    // console.log('sensor value is ' + value);

    // sensor write to memory address
    sensor.write(memaddr, msgbuf);

    // sensor read from memory address
    var vol = sensor.read(memaddr, 2);
    console.log("sensor read mem vol is ");
    console.log(vol);
    sensor.close();
}
