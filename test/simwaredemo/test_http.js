var http = require("http.js");
var fs = require("fs.js");

function test(){
    console.log("************** HTTP MODULE **************");
    http.request({
        method: "GET",
        url: "http://www.baidu.com",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json;charset:utf-8;",
            // "charsets: utf-8", // 这是一个错误的写法，详情：https://www.cnblogs.com/yanggb/p/11684494.html
        },
        responseType: "text",
        timeout: 10000,
        success (res) {
            console.log("[success] callback code:" + res.statusCode);
            console.log("[success] callback length:" + res.data.length);
            console.log("[success] callback config:"+ res.config);
            console.log(res.data);
        },
        fail (res) {
            console.log("[fail] callback code:" + res.statusCode);
            console.log("[fail] callback length:" + res.data.length);
            console.log("[fail] callback config:" + res.config);
        },
        complete(res) {
            console.log("[complete] callback code:" + res.statusCode);
            console.log("[complete] callback length:" + res.data.length);
            console.log("[complete] callback config:"+ res.config);
        }
    });

    http.request({
        method: "GET",
        url: "http://www.weather.com.cn/data/sk/101010100.html",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json;charsets:utf-8",
        },
        responseType: "text",
        timeout: 10000,
        success (res) {
            console.log("[success] callback code:" + res.statusCode);
            console.log("[success] callback length:" + res.data.length);
            console.log("[success] callback config:"+ res.config);
            console.log(res.data);
        },
        fail (res) {
            console.log("[fail] callback code:" + res.statusCode);
            console.log("[fail] callback length:" + res.data.length);
            console.log("[fail] callback config:"+ res.config);
        },
        complete(res) {
            console.log("[complete] callback code:" + res.statusCode);
            console.log("[complete] callback length:" + res.data.length);
            console.log("[complete] callback config:"+ res.config);
        }
    });

    http.request({
        method: "POST",
        url: "https://httpbin.org/post",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json;charsets: utf-8",
        },
        data: {
            a: 1,
            b: 2,
            c: 3,
        },
        responseType: "text",
        timeout: 10000,
        success (res) {
            console.log("[success] callback code:" + res.statusCode);
            console.log("[success] callback length:" + res.data.length);
            console.log("[success] callback config:"+ res.config);
            console.log(res.data);
        },
        fail (res) {
            console.log("[fail] callback code:" + res.statusCode);
            console.log("[fail] callback length:" + res.data.length);
            console.log("[fail] callback config:"+ res.config);
        },
        complete(res) {
            console.log("[complete] callback code:" + res.statusCode);
            console.log("[complete] callback length:" + res.data.length);
            console.log("[complete] callback config:"+ res.config);
        }
    });

    // var dataList = fs.readFile("data-list.json", "rb");
    // if (dataList) {
    //     dataList = JSON.parse(dataList);

    //     console.log("request total =======> " + dataList.length);
    //     // utils.logger.error("request total =======> " + dataList.length)

    //     var count = 0;
    //     globalThis.index = 0;
    //     for (var i = 0; i < dataList.length; i++) {
    //         console.log(
    //             "request index =======> " + i + "   " + JSON.stringify(dataList[i]["params"])
    //         );
            
    //         http.request({
    //             method: dataList[i]["method"],
    //             url: dataList[i]["url"],
    //             headers: {
    //                 "Accept": "application/json",
    //                 "Content-Type": "application/json;charsets: utf-8",
    //             },
    //             data: dataList[i]["params"],
    //             timeout: 10000,
    //             success (res) {
    //                 console.log("[success] callback code:" + res.statusCode);
    //                 console.log("[success] callback length:" + res.data.length);
    //                 console.log("[success] callback config:"+ res.config);
    //             },
    //             fail (res) {
    //                 console.log("[fail] callback code:" + res.statusCode);
    //                 console.log("[fail] callback length:" + res.data.length);
    //                 console.log("[fail] callback config:"+ res.config);
    //             },
    //             complete (res) {
    //                 console.log("[complete] callback code:" + res.statusCode);
    //                 console.log("[complete] callback length:" + res.data.length);
    //                 console.log("[complete] callback config:"+ res.config);
    //                 globalThis.index ++;
    //                 console.log("request finished================ =====>" + globalThis.index);

    //             }
    //         });
    //     }
    //     console.log("request finished, total success =====>" + count);
    // }
}
