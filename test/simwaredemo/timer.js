var ntimer = require("@native.timer");
require("eventbus.js");

function HWTIMER(id) {
    this.id = id;
    this.setTimeout = function (callback, timeout) {
        globalThis.eventbus.on("timer", "timeout", callback);
        ntimer.setTimeout(this.id, timeout);
    };

    this.setInterval = function (callback, timeout) {
        globalThis.eventbus.on("timer", "interval", callback);
        ntimer.setInterval(this.id, timeout);
    };

    this.close = function () {
        ntimer.close(this.id);
    };
}

function open(id) {
    if (id == undefined || typeof id != "number") return undefined;
    return new HWTIMER(id);
}
