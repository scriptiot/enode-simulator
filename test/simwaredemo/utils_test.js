var utils = require("./utils.js");

var options = {
    url: "",
    method: "",
    data: null,
    headers: {},
    timeout: 0
}

console.log(utils.validate(options, {
    url: {
        type: "string",
        allowNull: false,
        allowUndefined: false,
        allowEmpty: false,
    },
    method: {
        type: "string",
        allowNull: true,
        allowUndefined: false,
        allowEmpty: true,
        default: "GET"
    },
    timeout: {
        type: "number",
        allowNull: true,
        allowUndefined: true,
        default: 100
    },
    headers: {
        type: "object",
        allowNull: false,
        allowUndefined: false,
        allowEmpty: true,
        default: {}
    }
}))