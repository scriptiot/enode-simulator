var adc = require("adc.js");
os = require("@system.os");

function test(){
    console.log("**************** ADC MODULE **************")
    // voltage
    var vol = adc.open({
        "id": 4,
        "rate":10
    });

    var vol2 = adc.open({
        "id": 5,
        "rate":10
    });
  
    // read voltage
    var cnt = 10;
    while(cnt>0){
        var value = vol.read();
        console.log('voltage value is ' + value);

        value = vol2.read();
        console.log('voltage value is ' + value);
        os.delay(100);
        cnt--;
    }
    vol.close();
    vol2.close();
}
