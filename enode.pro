TEMPLATE = app
TARGET = enode
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CFLAGS += -Wall -Wshadow -Wundef -Wuninitialized -Wno-missing-braces

INCLUDEPATH += $$PWD/src

include(components/curl/libcurl.pri)
include(components/enode-modules/evm_module.pri)
include(components/libenode/libenode.pri)

SOURCES += \
    src/main.c \
    src/app_start.c
